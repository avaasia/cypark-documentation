function [ LON, LAT, ALT] = ExtractGPS(img)
img_meta = imfinfo(['RefImages\' img]);
img_gps = img_meta.GPSInfo;
LAT = img_gps.GPSLatitude(1) + (img_gps.GPSLatitude(2)/60) + (img_gps.GPSLatitude(3)/3600);
LON = img_gps.GPSLongitude(1) + (img_gps.GPSLongitude(2)/60) + (img_gps.GPSLongitude(3)/3600);
ALT = img_gps.GPSAltitude;
end

