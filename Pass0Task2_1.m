%% Tasks: 0.2
% Read the blueprint, and organize the panels into arrays, rows and columns
% Required:
    % Blueprint of area of interest
%%
clc, clear all, close all;

% Blueprint Grid
I = imread('Block1.jpg');

% Convert to binary
BW = im2bw(I);
figure, imshow(BW);
hold on;

%% Identify the solar panel and array boundaries
[B, ~] = bwboundaries(BW, 'holes');     % boundaries of panels and arrays
[C, ~] = bwboundaries(BW, 'noholes');   % boundaries of panels only

% Exclude the image contour
B(1,:) = [];
C(1,:) = [];
%%
D = {};
a = 1;

for i = 1:length(B)
    for j = 1:length(C)
        tf = isequal(B{i}, C{j});
        if tf ==1 
            break
        end
    end
    if tf == 0 && length(B{i}) > 100 % to avoid false positives
        D{a} = B{i};
        a = a+1;
        tf = 1;
    end
end

noofPanels = length(C);
noofArrays = length(D);

%% Plot to check everything is working
    % panels outlined in red
    % arrays outlined in blue

for i = 1:noofPanels
    boundary = C{i};
    plot(boundary(:,2), boundary(:,1), 'r', 'LineWidth', 1);
end
%%
for i = 1:noofArrays
    boundary = D{i};
    plot(boundary(:,2), boundary(:,1), 'b', 'LineWidth', 2);
end
hold off;
%% Identify the order of each array
   % note that MATLAB identifies boundaries using first column method
   % which might not always fit human requirements

% Identify the array with the left most pixel
% Find the last pixel value of this array (segmentation value)
% Find the arrays whose start column-pixel value is BEFORE the segmentation
% Sort arrays by row

% Get the start and end values of each solar array
arrayLimits = zeros(2,2,noofArrays);
for i = 1:noofArrays
    minrc = min(D{i});
    arrayLimits(1,1,i) = minrc(1);
    arrayLimits(1,2,i) = minrc(2);
    maxrc = max(D{i});
    arrayLimits(2,1,i) = maxrc(1);
    arrayLimits(2,2,i) = maxrc(2);
end

%% Sort the arrays by columns
   % E.g. Berjaya plant has 2 columns of arrays

columns = {};
a = 1;
noofRemaining = noofArrays;
remaining_arrays = arrayLimits;
d = 0;

% While loop will identify noof columns, and sort arrays accordingly
% Loop is necessary for grid with more than 2 columns

while noofRemaining > 0
    % Get the array with the left most pixel
    [~, arr] = min(remaining_arrays(1,2,:));
    % Identify the end value of this array
    segregation = remaining_arrays(2,2,arr);
    % Find the arrays which start before segregation value
    arrayStartCol = reshape(remaining_arrays(1,2,:), [noofRemaining, 1]);
    columns{a} = find(arrayStartCol < segregation) + d;
    % Repeat until all columns are identified
    remaining_arrays_idx = find(arrayStartCol > segregation);
    noofRemaining = length(remaining_arrays_idx);
    remaining_arrays = remaining_arrays(:,:, remaining_arrays_idx);
    d = d+length(columns{a});
    a = a+1;
end

%% For each column, sort arrays by rows

noofCols = length(columns);
order = cell(noofCols, 1);
d = 0;

for i = 1:noofCols
    col = columns{i};
    arrays = arrayLimits(:,:,col);
    [~, o] = sort(arrays(1,1,:));
    o = reshape(o, [length(o), 1])+d;
    order{i} = o;
    d = d + length(o);
end
order = cell2mat(order);
arraysOrdered = arrayLimits(:,:,order);

%% To locate the parent of each panel

panelLimits = zeros(2,2,noofPanels);
for i = 1:noofPanels
    minrc = min(C{i});
    panelLimits(1,1,i) = minrc(1);
    panelLimits(1,2,i) = minrc(2);
    maxrc = max(C{i});
    panelLimits(2,1,i) = maxrc(1);
    panelLimits(2,2,i) = maxrc(2);
end

panelParent = zeros(noofPanels, 1);
panelRow = zeros(noofPanels,1);
panelCol = zeros(noofPanels,1);

for i = 1:noofPanels
    panel = panelLimits(:,:,i);
    for j = 1:noofArrays
        AL = arraysOrdered(:,:,j);
        if panel(1, :) > AL(1,:) & panel(2,:) < AL(2,:)
            panelParent(i) = j;
            break
        end
    end
end

%% To sort the panels within each array

    % Identify the number of rows and columns in each solar array
arrayRowInfo = {};
arrayColInfo = {};
arrayDimensions = {};
a = 1;
for i = 1:noofArrays
    % extract the panels belonging to each array parent
    idx = find(panelParent == i);
    panels_in_array = panelLimits(:,:,idx);
    % find the number of unique starting rows
    rowStartPts = unique(panels_in_array(1,1,:));
    rowEndPts = unique(panels_in_array(2,1,:));
    noofRows = length(rowStartPts);
    % find the number of unique starting cols
    colStartPts = unique(panels_in_array(1,2,:));
    colEndPts = unique(panels_in_array(2,2,:));
    noofCols = length(colStartPts);
    % store the information
    arrayRowInfo{a} = [rowStartPts, rowEndPts];
    arrayColInfo{a} = [colStartPts, colEndPts];
    arrayDimensions{a} = [noofRows, noofCols];
    a = a+1;
end

for i = 1:noofPanels
    panel = panelLimits(:,:,i);
    %extract parent information
    parent = panelParent(i);
    rowStartPts = arrayRowInfo{parent}(:,1);
    rowEndPts = arrayRowInfo{parent}(:,2);
    colStartPts = arrayColInfo{parent}(:,1);
    colEndPts = arrayColInfo{parent}(:,2);
    
    % figure out which row and column
    panelRow(i) = find(rowStartPts == panel(1,1));
    panelCol(i) = find(colStartPts == panel(1,2));
end
panelInfo = [panelParent, panelRow, panelCol];

%% Clear unnecessary variables and save the workspace

clear a AL arr arrayLimits arrays arraysOrdered arrayStartCol boundary;
clear col colEndPts colStartPts d i idx j maxrc minrc noofRemaining noofRows noofCols;
clear o order panel panels_in_array parent remaining_arrays remaining_arrays_idx;
clear panelRow panelCol panelParent rowEndPts rowStartPts segregation tf;

savefig('Blueprint Sorted Figure.fig');
save('Blueprint Sorted Information.mat');
