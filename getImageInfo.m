function [ imageInfo ] = getImageInfo
    imageInfo = struct;
    
    cd('Images');
    files = dir('*.JPG');
    nooffiles = numel(files);
    imageInfo = nooffiles;
    
%     Identify the starting image number
%     filename = files(1).name;
%     start_num = strsplit(filename, '_');
%     start_num = strsplit(start_num{2}, '.');
%     start_num = str2double(start_num{1}) - 1;
    
%% This section gathers the gps information and time stamp in image metadata
    % matlab is able to access gps and timestamp info directly
    % also calculates difference in distance and time between subsequent
    % images
    
    % Initialize Empty Arrays
    latitudes = zeros(nooffiles, 1);
    longitudes = zeros(nooffiles, 1);
    altitudes = zeros(nooffiles, 1);
    distances = zeros(nooffiles, 1);
    times = zeros(nooffiles, 1); 
    times_elapsed = zeros(nooffiles, 1);
    
    % Initialize first values
    img_prev = imfinfo(files(1).name);
    gps_prev = img_prev.GPSInfo;
    lat_prev = gps_prev.GPSLatitude(1) + (gps_prev.GPSLatitude(2)/60) + (gps_prev.GPSLatitude(3)/3600);
    lon_prev = gps_prev.GPSLongitude(1) + (gps_prev.GPSLongitude(2)/60) + (gps_prev.GPSLongitude(3)/3600);
    alt_prev = gps_prev.GPSAltitude;
    
    latitudes(1) = lat_prev;
    longitudes(1) = lon_prev;
    altitudes(1) = alt_prev;

    time_prev = img_prev.DateTime;
    time_prev_datenum = datenum(time_prev, 'yyyy:mm:dd HH:MM:SS');
    times(1) = time_prev_datenum;
    
    % Iterate for remaining images
    for i = 2:nooffiles
        img = imfinfo(files(i).name);
        gps = img.GPSInfo;
        lat = gps.GPSLatitude(1) + (gps.GPSLatitude(2)/60) + (gps.GPSLatitude(3)/3600);
        lon = gps.GPSLongitude(1) + (gps.GPSLongitude(2)/60) + (gps.GPSLongitude(3)/3600);
        alt = gps.GPSAltitude;
        
        latitudes(i) = lat;
        longitudes(i) = lon;
        altitudes(i) = alt;
        distances(i) = 1000*pos2dist(lat_prev, lon_prev, lat, lon, 1);
        
        time = img.DateTime;
        time_datenum = datenum(time, 'yyyy:mm:dd HH:MM:SS');
        times(i) = time_datenum;
        times_elapsed(i) = round((time_datenum - time_prev_datenum)*86400);
        
        lat_prev = lat;
        lon_prev = lon;
        time_prev_datenum = time_datenum;
    end
    
    % Store information
    imageInfo.LON = longitudes;
    imageInfo.LAT = latitudes;
    imageInfo.ALTABS = altitudes;
    imageInfo.DIST = distances;
    imageInfo.TIME = times;
    imageInfo.TIMEELAPSED = times_elapsed;

%%  This section gathers the tri-axial velocity, drone RPY, and camera RPY
   % matlab cannot access this directly, so need to use exiftool
   % Initialize Empty Arrays
    speeds_X = zeros(nooffiles, 1);
    speeds_Y = zeros(nooffiles, 1);
    speeds_Z = zeros(nooffiles, 1);

    rolls_drone = zeros(nooffiles, 1);
    pitches_drone = zeros(nooffiles, 1);
    yaws_drone = zeros(nooffiles, 1);

    rolls_camera = zeros(nooffiles, 1);
    pitches_camera = zeros(nooffiles, 1);
    yaws_camera = zeros(nooffiles, 1);
    
    for i = 1:nooffiles
        [status, cmdout] = system(['exiftool -DJI:all ' files(i).name], '-echo');
        
        if status ~= 0
            disp('Error in extracting angle and heading information');
            break
        end
        
        cmds = strsplit(cmdout, '\n');
        check = cellfun('isempty',cmds);
        
        if check(1) == 0
        % Speed X
        speedX = strsplit(cmds{2}, ': ');
        speeds_X(i) = str2double(speedX{2});
        
        % Speed Y 
        speedY = strsplit(cmds{3}, ': ');
        speeds_Y(i) = str2double(speedY{2});
        
        % Speed Z 
        speedZ = strsplit(cmds{4}, ': ');
        speeds_Z(i) = str2double(speedZ{2});
        
        % Drone pitch
        pitch = strsplit(cmds{5}, ': ');
        pitches_drone(i) = str2double(pitch{2});
    
        % Drone yaw
        yaw = strsplit(cmds{6}, ': ');
        yaws_drone(i) = str2double(yaw{2});
    
        % Drone roll
        roll = strsplit(cmds{7}, ': ');
        rolls_drone(i) = str2double(roll{2});
        
        % Camera pitch
        pitch = strsplit(cmds{8}, ': ');
        pitches_camera(i) = str2double(pitch{2});
    
        % Camera yaw
        yaw = strsplit(cmds{9}, ': ');
        yaws_camera(i) = str2double(yaw{2});
    
        % Camera roll
        roll = strsplit(cmds{10}, ': ');
        rolls_camera(i) = str2double(roll{2});
        
        else 
            break
           % pitches_camera(i) = fnExtractExifInfo(files(i).name)
        end
    end
    cd('..');
    info = fnExtractExifInfo;
    pitches_camera = info.CP;
    % Store Information
    imageInfo.XV = speeds_X;
    imageInfo.YV = speeds_Y;
    imageInfo.ZV = speeds_Z;
    imageInfo.DP = pitches_drone;
    imageInfo.DY = yaws_drone;
    imageInfo.DR = rolls_drone;
    imageInfo.CP = pitches_camera;
    imageInfo.CY = yaws_camera;
    imageInfo.CR = rolls_camera;
end

