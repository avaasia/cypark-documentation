% 1) Identify solar panels from the images
% 2) Compare the thermal values in the solar panel and identify if there is a
%    significant within the panel
% 3) Index the panel that has problem

clear all;
%% Read the image file
I = imread('Capture.png');
I_R = I(:,:,1);
I_G = I(:,:,2);
I_B = I(:,:,3);
imshow(I);

%%
for i=1:size(I,1)
    for j=1:size(I,2)
        % remove black first
        if I_R(i,j) <= 10 && I_G(i,j) <= 10 && I_B(i,j) <= 10
            I_bw(i,j) = 0;
        elseif I_B(i,j) < I_R(i,j) && I_B(i,j) < I_G(i,j)
            I_bw(i,j) = 1;
        %%elseif I_R(i,j) < 100 && I_G(i,j) < 100 && I_B(i,j) < 100
            %%I_bw(i,j) = 1;
        else
            I_bw(i,j) = 0;
        end
    end
end
imshow(I_bw);
%% Change RGB to HSV c = L==3;
%{
I_HSV = rgb2hsv(I);

% Extract each channel of HSV
I_H = I_HSV(:,:,1);
I_S = I_HSV(:,:,2);
I_V = I_HSV(:,:,3);
%}

%% Convert image to grey scale
%%I_gray = rgb2gray(I);

%% Adjust the contrast of the grey scale image
%I_adjust = imadjust(I_gray);

%% Turn the grey scale image to binary
%%I_bw = im2bw(I_gray,0.3);
%%imshow(I_bw);

%% Inverse the binary image
for i=1:size(I_bw,1)
    for j=1:size(I_bw,2)
        if I_bw(i,j) == 1
            I_bw(i,j) = 0;
        else
            I_bw(i,j) = 1;
        end
    end
end
imshow(I_bw);

%% remove small holes

I_bw = bwareaopen(I_bw, 50);
imshow(I_bw);

%% Fill up the holes of the image
I_fill = imfill(I_bw, 'holes');
imshow(I_fill);

%% Eroding 
SE = strel('rectangle', [4 1]);
I_fill = imerode(I_fill, SE);
imshow(I_fill);

SE = strel('rectangle', [1 5]);
I_fill = imerode(I_fill, SE);
imshow(I_fill);

I_fill = bwareaopen(I_fill, 100);
imshow(I_fill);

%% Dilating
SE = strel('rectangle', [5 1]);
I_fill = imdilate(I_fill, SE);
imshow(I_fill);

SE = strel('rectangle', [1 5]);
I_fill = imdilate(I_fill, SE);
imshow(I_fill);

%% Shape based filter
SE = strel('rectangle', [5 1]);
I_fill = imclose(I_fill, SE);
imshow(I_fill);

%% Shape based filter
SE = strel('rectangle', [1 5]);
I_fill = imclose(I_fill, SE);
imshow(I_fill);

%% Shape based filter

SE = strel('line', 10, 85);
I_fill = imclose(I_fill, SE);
imshow(I_fill);


%% Detect the edges of the image
%{
I_edge = edge(I_openned, 'Canny');
imshow(I_edge);
%}

%% Inverse the image
%{
for i=1:size(I_openned,1)
    for j=1:size(I_openned,2)
        if I_openned(i,j) == 1
            I_openned(i,j) = 0;
        else
            I_openned(i,j) = 1;
        end
    end
end

%% Detect the boundary of the panels
[B,L] = bwboundaries(I_openned, 'holes');
[C,M] = bwboundaries(I_openned, 'noholes');
imshow(I_openned);

%% Remove the non-closing edges
D = {};
a = 1;
     
for i=1:length(B)
    for j=1:length(C)
        tf = isequal(B{i},C{j});
        if tf == 1
            break
        end
    end
    if tf == 0
        D{a} = B{i};
        a = a+1;
        tf = 1;
    end
end

%% To find the left pixel and right pixel of the boundary of each panel
for z=1:length(D)
    sortedD = sortrows(D{z});
    a = 0;
    y = 0;
    for i=1:length(sortedD)
        if sortedD(i,1)>y
            a = a+1;
            E{a,1} = sortedD(i,:);
            x_left = sortedD(i,2);
            y = sortedD(i,1);
        elseif sortedD(i,1)==y
            if sortedD(i,2)>x_left
                E{a,2} = sortedD(i,:);
            end
        end
    end
    F{z,1} = E;
    E={};
end

%% To find the 4 corners of the panels

for d=1:length(D)
   
    % Find the top corner
    top_min_y = min(D{1,d}(:,1));
    top_min_x = 0;
    for i=1:length(D{1,d})
        if D{1,d}(i,1)~= top_min_y
            continue
        end
        if top_min_x==0
            top_min_x = D{1,d}(i,2);
        end
        if D{1,d}(i,2)<top_min_x
            top_min_x = D{1,d}(i,2);
        end
    end

    % Find the bottom corner
    bot_max_y = max(D{1,d}(:,1));
    bot_max_x = 0;
    for i=1:length(D{1,d})
        if D{1,d}(i,1)~= bot_max_y
            continue
        end
        if bot_max_x==0
            bot_max_x = D{1,d}(i,2);
        end
        if D{1,d}(i,2)>bot_max_x
            bot_max_x = D{1,d}(i,2);
        end
    end

    % Find the left corner
    left_min_x = min(D{1,d}(:,2));
    left_max_y = 0;
    for i=1:length(D{1,d})
        if D{1,d}(i,2)~= left_min_x
            continue
        end
        if left_max_y==0
            left_max_y = D{1,d}(i,1);
        end
        if D{1,d}(i,1)>left_max_y
            left_max_y = D{1,d}(i,1);
        end
    end

    % Find the right corner
    right_max_x = max(D{1,d}(:,2));
    right_min_y = 0;
    for i=1:length(D{1,d})
        if D{1,d}(i,2)~= right_max_x
            continue
        end
        if right_min_y==0
            right_min_y = D{1,d}(i,1);
        end
        if D{1,d}(i,1)<right_min_y
            right_min_y = D{1,d}(i,1);
        end
    end
    
    % Compile the coordinates of 4 corners
    G{d,1}(1,1) = top_min_x;
    G{d,1}(1,2) = top_min_y;
    G{d,1}(1,3) = bot_max_x;
    G{d,1}(1,4) = bot_max_y;
    G{d,1}(1,5) = left_min_x;
    G{d,1}(1,6) = left_max_y;
    G{d,1}(1,7) = right_max_x;
    G{d,1}(1,8) = right_min_y;

    
    % To determine the length from corner to another
    length_panel{d,1}(1,1) = sqrt((G{d,1}(1,1)-G{d,1}(1,5))^2+(G{d,1}(1,2)-G{d,1}(1,6))^2);
    length_panel{d,1}(2,1) = sqrt((G{d,1}(1,5)-G{d,1}(1,3))^2+(G{d,1}(1,6)-G{d,1}(1,4))^2);
    length_panel{d,1}(3,1) = sqrt((G{d,1}(1,3)-G{d,1}(1,7))^2+(G{d,1}(1,4)-G{d,1}(1,8))^2);
    length_panel{d,1}(4,1) = sqrt((G{d,1}(1,1)-G{d,1}(1,7))^2+(G{d,1}(1,2)-G{d,1}(1,8))^2);

    % To determine the pixels size for a solar cell
    cellV(d) = length_panel{d,1}(1,1)/10;
    cellH(d) = length_panel{d,1}(2,1)/6;

 %To determine cell level boundary
    
    % To generate cell level boundary
    
    % First line cell coordinate
    for i=1:11
        if i == 1
            Panel_Cell_Boundary_y{d,1}(i,1) = G{d,1}(1,2);
        elseif i == 11
            Panel_Cell_Boundary_y{d,1}(i,1) = G{d,1}(1,6);
        else 
            Panel_Cell_Boundary_y{d,1}(i,1) = floor(Panel_Cell_Boundary_y{d,1}(1,1)...
                + (i-1)*cellV(d));
        end
    end
    
    for i=1:11
        if i == 1
            Panel_Cell_Boundary_x{d,1}(i,1) = G{d,1}(1,1);
        elseif i == 11
            Panel_Cell_Boundary_x{d,1}(i,1) = G{d,1}(1,5);
        else 
            for j=1:length(F{d,1})
                if Panel_Cell_Boundary_y{d,1}(i,1) ~= F{d,1}{j,1}(1,1);
                    continue
                else
                    Panel_Cell_Boundary_x{d,1}(i,1) = F{d,1}{j,1}(1,2);
                end
            end
        end
    end
    
    % Last line cell coordinate
    for i=1:11
        if i == 1
            Panel_Cell_Boundary_y{d,1}(i,7) = G{d,1}(1,8);
        elseif i == 11
            Panel_Cell_Boundary_y{d,1}(i,7) = G{d,1}(1,4);
        else 
            Panel_Cell_Boundary_y{d,1}(i,7) = floor(Panel_Cell_Boundary_y{d,1}(1,7)...
                + (i-1)*cellV(d));
        end
    
    end

    for i=1:11
        if i == 1
            Panel_Cell_Boundary_x{d,1}(i,7) = G{d,1}(1,7);
        elseif i == 11
            Panel_Cell_Boundary_x{d,1}(i,7) = G{d,1}(1,3);
        else 
            for j=1:length(F{d,1})
                if Panel_Cell_Boundary_y{d,1}(i,1) ~= F{d,1}{j,2}(1,1);
                    continue
                else
                    Panel_Cell_Boundary_x{d,1}(i,7) = F{d,1}{j,2}(1,2);
                end
            end
        end
    end
    
 % Internal cell coordinate

    % cell boundary y coordinate
    for i=1:11
        Vector_cellY(d,1) = Panel_Cell_Boundary_y{d,1}(1,7) - ...
            Panel_Cell_Boundary_y{d,1}(1,1);
        for j=2:length(Panel_Cell_Boundary_y{d,1}(1,:))-1
            Panel_Cell_Boundary_y{d,1}(i,j) = Panel_Cell_Boundary_y{d,1}(i,1)+ ...
                round((j-1)/(length(Panel_Cell_Boundary_y{d,1}(1,:))-1)...
                *Vector_cellY(d));
        end
    end

    % cell boundary x coordinate
    for i=1:11
        Vector_cellX(d,1) = Panel_Cell_Boundary_x{d,1}(1,7) - ...
            Panel_Cell_Boundary_x{d,1}(1,1);
        for j=2:length(Panel_Cell_Boundary_x{d,1}(1,:))-1
            Panel_Cell_Boundary_x{d,1}(i,j) = Panel_Cell_Boundary_x{d,1}(i,1)+ ...
                round((j-1)/(length(Panel_Cell_Boundary_x{d,1}(1,:))-1)...
                *Vector_cellX(d));
        end
    end
    
    % full cell boundary

    for c=1:length(Panel_Cell_Boundary_y{d,1}(1,:))
        a = 1;
        for i=1:length(Panel_Cell_Boundary_y{d,1}(:,1))-1
            for j=Panel_Cell_Boundary_y{d,1}(i,c):Panel_Cell_Boundary_y{d,1}(i+1,c)-1
               Cell_Boundary_l{d,1}(a,c*2) = j;
               Cell_Boundary_l{d,1}(a,c*2-1) = Panel_Cell_Boundary_x{d,1}(i,c);
               a = a+1;
            end
            %to get the last value
            if isempty(j) == 1
                continue
            end
            Cell_Boundary_l{d,1}(a,c*2-1) = Panel_Cell_Boundary_x{d,1}(i,c);
            Cell_Boundary_l{d,1}(a,c*2) = j+1; 
        end
    end

    for i=1:length(Panel_Cell_Boundary_x{d,1}(:,1))
        a = 1;
        for c=1:length(Panel_Cell_Boundary_x{d,1}(1,:))-1
            for j=Panel_Cell_Boundary_x{d,1}(i,c):Panel_Cell_Boundary_x{d,1}(i,c+1)
                Cell_Boundary_s{d,1}(a,i*2-1) = j;
                Cell_Boundary_s{d,1}(a,i*2) = Panel_Cell_Boundary_y{d,1}(i,c);
                a = a+1;
            end
        end
    end
    
    a=1;
    for j=1:length(Cell_Boundary_l{d,1}(1,:))
       for i=1:length(Cell_Boundary_l{d,1}(:,1))
           if mod(j,2) == 0
                continue
           else
                if Cell_Boundary_l{d,1}(i,j) == 0
                    continue
                elseif Cell_Boundary_l{d,1}(i,j+1) == 0
                    continue
                end
              Complete_Boundary{d,1}(a,1) = Cell_Boundary_l{d}(i,j);
              Complete_Boundary{d,1}(a,2) = Cell_Boundary_l{d}(i,j+1);
           end
              a=a+1;
        end
    end

    for j=1:length(Cell_Boundary_s{d,1}(1,:))
       for i=1:length(Cell_Boundary_s{d,1}(:,1))
                if mod(j,2) == 0
                    continue
                else
                    if Cell_Boundary_s{d,1}(i,j) == 0
                        continue
                    end
                    Complete_Boundary{d,1}(a,1) = Cell_Boundary_s{d,1}(i,j);
                    Complete_Boundary{d,1}(a,2) = Cell_Boundary_s{d,1}(i,j+1);
                end
                a=a+1;
        end
    end
    
    
    % to determine the individual box boundary
    
    for i=1:length(Panel_Cell_Boundary_y{d}(:,1))-1
        for l=1:length(Panel_Cell_Boundary_y{d}(1,:))-1
        b=1;
                for k=Panel_Cell_Boundary_y{d}(i,l):Panel_Cell_Boundary_y{d}(i+1,l)
                    if k == 0
                        break
                    end                      
                    % to get the y coordinate of left boundary
                    Box_Boundary{d,1}{i,l}(b,2) = k;
                    % to get the x coordinate of left boundary
                    Box_Boundary{d,1}{i,l}(b,1) = Panel_Cell_Boundary_x{d,1}(i,l);
                    % to get the y coordinate of left boundary
                    Box_Boundary{d,1}{i,l}(b,4) = k;
                    % to get the x coordinate of right boundary
                    Box_Boundary{d,1}{i,l}(b,3) = Panel_Cell_Boundary_x{d,1}(i,l+1);
                    b = b+1;                    
                end
                
        end
    end
end
    
 
%% Read thermal image
IR = imread('P3Thermal.jpg');
IR_gray = IR(:,:,1);
imtool(IR);

%% Scale IR image to the temperature
IR_double = im2double(IR_gray); %convert from uint to double
T_min = 36;
Tp_max = 52;
T_diff = Tp_max - T_min;
IR_scaled = IR_double*T_diff + T_min; %scale the image to the min and max

%% Calculate mean temperature within the panel
for k=1:size(F,1)
    panel = F{k};
    T_sum = 0;
    count = 0;
    m=1;
    T_min(k) = 0;
    T_max(k) = 0;
    tr = 1;
    for i=panel{1,1}(1,1):panel{length(panel),1}(1,1)          
        for j=panel{m,1}(1,2):panel{m,2}(1,2)
            % find the maximum temperature in the panel
            if IR_scaled(i,j)>T_max(k)
                T_max(k) = IR_scaled(i,j);
            end
            % find the minimum temperature in the panel
            if count==0
                T_min(k) = IR_scaled(i,j); 
            elseif IR_scaled(i,j)<T_min(k)
                T_min(k) = IR_scaled(i,j);
            end
            moduleInfo{k}(tr,1) = i;
            moduleInfo{k}(tr,2) = j;
            moduleInfo{k}(tr,3) = IR_scaled(i,j);
            count = count+1;
            tr = tr + 1;
            if IR_scaled(i,j)>44
                %IR(i,j) = 0;
            end
        end
    m=m+1; %to loop through the panel pixel
    end
    % temperature
    T_diff(k) = T_max(k)-T_min(k);
    %T_mean(k) = mean(moduleInfo{k}(:,3));
    T_median(k) = median(moduleInfo{k}(:,3));
    T_var(k) = var(moduleInfo{k}(:,3));
    % to find the centroid of the panel
    centroid_x(k) = (panel{1,1}(1,2)+panel{length(panel),1}(1,2))/2;
    centroid_y(k) = (panel{1,1}(1,1)+panel{length(panel),1}(1,1))/2;
    centroid{k} = [centroid_x(k) centroid_y(k)];
end

%% Cell level checking

% to check cell level temperature
for a=1:length(Box_Boundary)
    for i=1:length(Box_Boundary{a,1}(:,1))
        for j=1:length(Box_Boundary{a,1}(1,:))
            for m=Box_Boundary{a,1}{i,j}(1,2):Box_Boundary{a,1}{i,j}...
                    (length(Box_Boundary{a,1}{i,j}(:,1)),2)
                x = Box_Boundary{a,1}{i,j}(1,2);
                y = Box_Boundary{a,1}{i,j}(1,1);
                
                for n=Box_Boundary{a,1}{i,j}(1,1):Box_Boundary{a,1}{i,j}(1,3)
                    T_cell{a,1}{i,j}(m-x+1,n-y+1) = IR_scaled(m,n);
                     
                end
            end
            T_cell_mean{a,1}(i,j) = mean(mean(T_cell{a,1}{i,j}));
            T_cell_max{a,1}(i,j) = max(max(T_cell{a,1}{i,j}));
        end
    end
    T_panel_mean(a,1) = mean(mean(T_cell_mean{a,1})); 
    T_panel_max(a,1) = max(max(T_cell_max{a,1}));
end

T_mean = mean(T_panel_mean);
T_mean_low = T_mean*0.95;
T_mean_high = T_mean*1.05;
T_cell_high = T_mean*1.08;
T_cell_low = T_mean*0.92;
% to flag out problematic cells

for a=1:length(T_cell_mean)
    for i=1:length(T_cell_mean{a,1}(:,1))
        for j=1:length(T_cell_mean{a,1}(1,:))
            if T_cell_mean{a,1}(i,j) > T_mean_high
                if T_cell_mean{a,1}(i,j) > T_cell_high
                    T_cell_flag{a,1}(i,j) = 2;
                else
                    T_cell_flag{a,1}(i,j) = 1;
                end
            elseif T_cell_mean{a,1}(i,j) < T_mean_low
                if T_cell_mean{a,1}(i,j) < T_cell_low
                    T_cell_flag{a,1}(i,j) = -2;
                else
                    T_cell_flag{a,1}(i,j) = -1;
                end
            else
                T_cell_flag{a,1}(i,j) = 0;
            end
            
            if T_cell_flag{a,1}(i,j) == 2
                for m=Box_Boundary{a,1}{i,j}(1,2):Box_Boundary{a,1}{i,j}...
                        (length(Box_Boundary{a,1}{i,j}(:,2)),2)
                    for n=Box_Boundary{a,1}{i,j}(1,1):Box_Boundary{a,1}{i,j}(1,3)
                        IR(m,n,1) = 255;
                        IR(m,n,2) = 0;
                        IR(m,n,3) = 0;
                    end
                    
                end  
            end
            
            if T_cell_flag{a,1}(i,j) == 1
                for m=Box_Boundary{a,1}{i,j}(1,2):Box_Boundary{a,1}{i,j}...
                        (length(Box_Boundary{a,1}{i,j}(:,2)),2)
                    for n=Box_Boundary{a,1}{i,j}(1,1):Box_Boundary{a,1}{i,j}(1,3)
                        IR(m,n,1) = 255;
                        IR(m,n,2) = 255;
                        IR(m,n,3) = 0;
                    end
                    
                end  
            end
            
            if T_cell_flag{a,1}(i,j) == -1
                for m=Box_Boundary{a,1}{i,j}(1,2):Box_Boundary{a,1}{i,j}...
                        (length(Box_Boundary{a,1}{i,j}(:,2)),2)
                    for n=Box_Boundary{a,1}{i,j}(1,1):Box_Boundary{a,1}{i,j}(1,3)
                        IR(m,n,1) = 0;
                        IR(m,n,2) = 255;
                        IR(m,n,3) = 255;
                    end
                    
                end  
            end

            if T_cell_flag{a,1}(i,j) == -2
                for m=Box_Boundary{a,1}{i,j}(1,2):Box_Boundary{a,1}{i,j}...
                        (length(Box_Boundary{a,1}{i,j}(:,2)),2)
                    for n=Box_Boundary{a,1}{i,j}(1,1):Box_Boundary{a,1}{i,j}(1,3)
                        IR(m,n,1) = 0;
                        IR(m,n,2) = 0;
                        IR(m,n,3) = 255;
                    end
                    
                end  
            end
            
        end
    end
end

%% Plot the images in a figure
subplot(2,2,1),imshow(I);
subplot(2,2,2),imshow(I_gray);
subplot(2,2,3),imshow(I);
hold on;
for k =1:length(D)
    boundary = D{k};
    plot(boundary(:,2),boundary(:,1),'g','LineWidth',2)
end
subplot(2,2,4),imshow(IR);
hold on;
for k =1:length(Complete_Boundary)
    boundary = Complete_Boundary{k};
    scatter(boundary(:,1),boundary(:,2),1)
end

%}

