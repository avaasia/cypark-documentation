function [ regProp ] = fnRegionProps( I, area, centroid, boundingBox, extrema )
%Extract the main properties of the blobs in an image
%   Panel Extraction from blueprint

[L,num] = bwlabel(I);
stats = regionprops(L, area, centroid, boundingBox, extrema);

% Extract information the panel
area = cat(1,stats.Area);
centroid = cat(1,stats.Centroid);
BoundingBox = cat(1,stats.BoundingBox);
Extrema = stats.Extrema;

regProp = struct('Area', area, 'Centroids', centroid, 'BoundingBox', ...
    BoundingBox, 'Extrema', Extrema )

%plot(regProp.Centroids(:,1), regProp.Centroids(:,2), 'r*');
%plot(regProp.BoundingBox(:,1), regProp.BoundingBox(:,2), 'b*');
end

