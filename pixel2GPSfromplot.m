function [imageGPS] = pixel2GPSfromplot() 
load('block2test_imageInfo.mat')
load('block2test_IMAGES.mat')
r  = load('reference_frameblock2.mat');  %testing code, must change
BL = r.frame.BL;
BR = r.frame.BR;
TR = r.frame.TR;
TL = r.frame.TL;
SP = r.frame.SP;
index = size(images.X);
x=1;
while x <= index(1)
latBL(x) = BL.LAT + (images.X(x)-BL.XP)*SP.XLAT + (images.Y(x)-BL.YP)*SP.YLAT;
lonBL(x) = BL.LON + (images.X(x)-BL.XP)*SP.XLON + (images.Y(x)-BL.YP)*SP.YLON;
x = x+1;
end
x=1;

while x <= index(1)
    %reference from bottom right corner
    latBR(x) = BR.LAT + (images.X(x)-BR.XP)*SP.XLAT + (images.Y(x)-BR.YP)*SP.YLAT;
    lonBR(x) = BR.LON + (images.X(x)-BR.XP)*SP.XLON + (images.Y(x)-BR.YP)*SP.YLON;
    x = x+1;
end

x=1;

while x <= index(1)
    %reference from top right corner
    latTR(x) = TR.LAT + (images.X(x)-TR.XP)*SP.XLAT + (images.Y(x)-TR.YP)*SP.YLAT;
    lonTR(x) = TR.LON + (images.X(x)-TR.XP)*SP.XLON + (images.Y(x)-TR.YP)*SP.YLON;
    x = x+1;
end

x=1;

while x <= index(1)
    %reference from top left corner
    latTL(x) = TL.LAT + (images.X(x)-TL.XP)*SP.XLAT + (images.Y(x)-TL.YP)*SP.YLAT;
    lonTL(x) = TL.LON + (images.X(x)-TL.XP)*SP.XLON + (images.Y(x)-TL.YP)*SP.YLON;
    x = x+1;
end

x=1;

while x <= index(1)
   lat(x) = ((latBL(x)+ latBR(x)+ latTR(x)+latTL(x))/4);
   lon(x) = ((lonBL(x)+ lonBR(x)+ lonTR(x)+ lonTL(x))/4);
   x = x+1;
end
ImageGPS = horzcat(lat',lon');
end