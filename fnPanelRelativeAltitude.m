function [ Alt ] = fnPanelRelativeAltitude( I )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    dist = fnCalculatePanelDistance(I);
    q1 = 'What is the panel length(m)?\n';
    l = input(q1);
    dist_px = l/dist;
    q2 = 'What is the Horizontal FOV of the lens(deg)?\n';
    H_FOV = input(q2);
    Alt = fnObjectDistanceFromDrone(dist_px,H_FOV);

end

