
function [ flightpath ] = ConvertFlightInfo( flightInfo, refFrame)

% coefficients for simutaneous equations
C1 = refFrame.SP.XLON;
C2 = refFrame.SP.XLAT;
C3 = refFrame.SP.YLON;
C4 = refFrame.SP.YLAT;

% calculate gps difference between target point and refernce point
DiffLON = flightInfo.LON - refFrame.BL.LON;
DiffLAT = flightInfo.LAT - refFrame.BL.LAT;

% initialize empty vectors to store information
pathpts = length(DiffLON);
Xpath = zeros(pathpts, 1);
Ypath = zeros(pathpts, 1);

% only calculates every 50 points, can change this number
% <50 - programme becomes slower, but more data points calculated
% >50 - programme becomes faster, but less data points calculated
% simultaneous equations will solve for pixels from GPS values
interval = 50;
for i = 1:interval:pathpts
    syms x y
    eqn1 = C1*x + C3*y == DiffLON(i);
    eqn2 = C2*x + C4*y == DiffLAT(i);
    
    [E1, E2] = equationsToMatrix([eqn1, eqn2], [x, y]);
    p_c = linsolve(E1, E2);
    Px = refFrame.BL.XP + double(p_c(1));
    Py = refFrame.BL.YP + double(p_c(2));
    
    % Store the pixel coordinates of target point
    Xpath(i) = Px;
    Ypath(i) = Py;
end

% remove the zeros
Xpath = Xpath(1:interval:pathpts);
Ypath = Ypath(1:interval:pathpts);

% store all relevant information
flightpath.X = Xpath;
flightpath.Y = Ypath;
flightpath.ALTABS = flightInfo.ALTABS(1:interval:pathpts);
flightpath.ALTREL = flightInfo.ALTREL(1:interval:pathpts);
end

