clear all;
clc;

I = imread('DJI_0003.jpg');
I = imadjust(I,[],[],2.2);

I_R = im2double(I(:,:,1));
I_G = im2double(I(:,:,2));
I_B = im2double(I(:,:,3));

I_Y = I_R + I_G + I_B;

r = I_R ./ I_Y;
g = I_G ./ I_Y;
b = I_B ./ I_Y;

T = b>0.4&r<0.3&I_Y>0.5; 

%{
SE = strel('square',10);
T = imdilate(T,SE);
%}

T = imfill(T,'holes');


V = bwareaopen(T, 70000);


SE = strel('square',5);
V = imdilate(V,SE);


U = imfill(V,'holes');

[L,num]= bwlabel(V);

SE = strel('square',50);

%{
for i =1:num
    c = L==i;
    d = imdilate(c,SE) .* I_B;
    imshow(d);
    figure;
end
%}

c = L==28;
ROI = imdilate(c,SE);

d = ROI .* b;
e = ROI .* r;
f = ROI .* g;
g = ROI .* I_Y;

U = g>1.6;
H = imfill(U,'holes');
ROI = bwareaopen(H, 70000);
imshow(ROI);
figure;
d = ROI .* b;
e = ROI .* r;
f = ROI .* g;
g = ROI .* I_Y;
imshow(d);

% Case 1: Black stains
Black = g<0.7 & d<0.4 & e<0.4;

% Case 2: Brown stains
Brown = g<1.65 & g>1 & d < 0.35 & e>d;
Brown_ROI = Brown .* g;
Black_ROI = Black .* g;

%Case 3:Black_Blue stains
%Black_blue = g<0.55 & d <0.45 & d>0.39;

Dirt_ROI = (Brown+Black+Black_blue) .* g;
Dirt_ROI = bwareaopen(Dirt_ROI, 10);
imshow(Dirt_ROI);

%{

stats = regionprops(L==1, 'all');

B = bwboundaries(L==1);


subplot(2,2,1),imshow(T);
subplot(2,2,2),imshow(L == 1);

subplot(2,2,3),imshow(L==1);
hold on;
plot(stats.Centroid(1), stats.Centroid(2), '*');
for k =1:length(stats.Extrema)
    boundary = stats.Extrema;
    scatter(boundary(:,1),boundary(:,2),100)
end
subplot(2,2,4),imshow(U);
%}

%}