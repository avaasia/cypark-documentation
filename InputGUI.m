function InputGUI()

fc = figure('Visible', 'off', 'HandleVisibility', 'on', 'Tag', 'calculator', ...
    'Position', [360, 500, 500, 220], 'Units', 'normalized');

ref_info = struct('IMG', 'str', 'LOC', 'str', 'XP', 0, 'YP', 0, 'LON', 0, 'LAT', 0, 'ALT',0);

frame.TL = ref_info;
frame.TR = ref_info;
frame.BL = ref_info;
frame.BR = ref_info;

guidata(fc, ref_info);

fwtext1 = uicontrol('Style', 'text', 'Position', [50, 100, 400, 100],...
    'String','Please input the GPS coordinates in terms of Latitude and Longitute with as much decimals as possible.', ...
    'FontSize', 11, 'ForegroundColor', 'r');

 fcmenu = uicontrol('Style', 'popupmenu', 'String', {'Choose Reference Point', ...
     'Bottom-Left', 'Top-Left', 'Top-Right' , 'Bottom-Right'},...
     'Position', [50, 30, 150, 95], 'FontSize', 10, 'Callback', {@Menu_Callback});
 
block_Notext = uicontrol('Style', 'text', 'String', 'Block Number', ...
    'Position', [98, 70, 120, 25], 'FontSize', 10, 'HorizontalAlignment', 'left');

BlockNo = uicontrol('Style', 'edit', 'String', '', ...
    'Position', [100, 50, 75, 25], 'FontSize', 10, 'HorizontalAlignment', 'left');
 
fclattext = uicontrol('Style', 'text', 'String', 'Latitude', ...
    'Position', [225, 120, 75, 25], 'FontSize', 10, 'HorizontalAlignment', 'left');

fclat = uicontrol('Style', 'edit', 'String', ' ', ...
    'Position', [215, 100, 75, 25], 'FontSize', 10, 'HorizontalAlignment', 'left');

fclontext = uicontrol('Style', 'text', 'String', 'Longitude', ...
    'Position', [305, 120, 75, 25], 'FontSize', 10, 'HorizontalAlignment', 'left');

fclon = uicontrol('Style', 'edit', 'String', ' ', ...
    'Position', [295, 100, 75, 25], 'FontSize', 10, 'HorizontalAlignment', 'left');

fccoord = uicontrol('Style', 'pushbutton', 'String', 'Store', ...
    'Position', [400, 100, 50, 25], 'FontSize', 10, ...
    'Callback', {@store_Callback});

xPxtext = uicontrol('Style', 'text', 'String', 'X pixel coordinate', ...
    'Position', [225, 70, 75, 25], 'FontSize', 10, 'HorizontalAlignment', 'left');

xPx = uicontrol('Style', 'edit', 'String', ' ', ...
    'Position', [215, 50, 75, 25], 'FontSize', 10, 'HorizontalAlignment', 'left');

yPxtext = uicontrol('Style', 'text', 'String', 'Y pixel coordinate', ...
    'Position', [305, 70, 75, 25], 'FontSize', 10, 'HorizontalAlignment', 'left');

yPx = uicontrol('Style', 'edit', 'String', ' ', ...
    'Position', [295, 50, 75, 25], 'FontSize', 10, 'HorizontalAlignment', 'left');

startrow = uicontrol('Style', 'pushbutton', 'String', 'Select Start Row', ...
    'Position', [180, 15, 150, 25], 'FontSize', 10, ...
    'Callback', {@startrow_Callback});

movegui(fc,[750 200])
fc.Name = 'Input GUI';
fc.Visible = 'on';

OutputFcn;

%initilisation to find the 4 reference points and plot them on the blueprint
    function OutputFcn()
    global Bottom_LeftCoord Right_TopCorner Right_BtmCorner Left_TopCoord;
    cobj = findobj('Tag', 'operator');
        if ~isempty(cobj)
            cdata = guidata(cobj)
            filename = cdata.imageName;
            Img = cdata.I;
            I = imread(Img);
            %set the filename based on block extracted
            filename = cdata.imageName;
            set(BlockNo, 'String',filename);
        end
         blueprint = fnAsBuiltExtraction( I );
        regProp = fnRegionProps( blueprint.bluePrint,'Area', 'Centroid', 'boundingBox', 'Extrema');

         %find which rows has the smallest x coordinate
        rowx= find(regProp.Centroids==min(regProp.Centroids(:,1)));
        
        %Find the corresponding y coordinate
        MinVal_Y=regProp.Centroids(rowx(1),2); 
        for x1 = 1:size(rowx,1)
            if regProp.Centroids(rowx(x1),2)<MinVal_Y; 
            MinVal_Y=regProp.Centroids(rowx(x1),2); 
            end
        end
        Left_TopCent = [min(regProp.Centroids(:,1)), MinVal_Y];
        indexLT = find(ismember(regProp.Centroids,Left_TopCent,'rows'));
        Left_TopCoord = [regProp.BoundingBox(indexLT),regProp.BoundingBox(indexLT,2)];
        
        %finding the top right corner coordinates
        rowTop= find(regProp.BoundingBox(indexLT,2)== regProp.BoundingBox(:,2));
        MaxVal_X = regProp.BoundingBox(indexLT);
         for x2 = 1:size(rowTop,1)
            if regProp.BoundingBox(rowTop(x2),1)> MaxVal_X; 
            MaxVal_X=regProp.BoundingBox(rowTop(x2),1); 
            end
        end
        Right_TopCorner = [MaxVal_X + regProp.BoundingBox(rowTop(x2),3), regProp.BoundingBox(indexLT,2)];
        
        %finding the bottom left corner coordinates
        rowy= find(regProp.Centroids(:,2)==max(regProp.Centroids(:,2)));
        MinVal_X=regProp.Centroids(1,1); 
        for x3 = 1:size(rowy,1)
            if regProp.Centroids(rowy(x3),1)<MinVal_X; 
            MinVal_X=regProp.Centroids(rowy(x3),1); 
            end
        end
        Bottom_LeftCent = [MinVal_X, max(regProp.Centroids(:,2))];
        indexBL = find(ismember(regProp.Centroids,Bottom_LeftCent,'rows'));
        Bottom_LeftCoord = [regProp.BoundingBox(indexBL), ...
            (regProp.BoundingBox(indexBL,2)+regProp.BoundingBox(indexBL,4))];
        
        %finding the bottom right corner coordinates
        rowBtm= find(regProp.BoundingBox(indexBL,2)== regProp.BoundingBox(:,2));
        MaxVal_X2 = regProp.BoundingBox(indexBL);
         for x4 = 1:size(rowBtm,1)
            if regProp.BoundingBox(rowBtm(x4),1)> MaxVal_X2; 
            MaxVal_X2=regProp.BoundingBox(rowBtm(x4),1); 
            end
        end
        Right_BtmCorner = [MaxVal_X2 + regProp.BoundingBox(rowBtm(x4),3), ... 
            (regProp.BoundingBox(indexBL,2)+regProp.BoundingBox(indexBL,4))];
       
        %plots the 4 reference points on the blueprint
        cobj = findobj('Tag', 'operator');
        figure (cobj);
        plot(Bottom_LeftCoord(1),Bottom_LeftCoord(2),'b.','MarkerSize',20);
        plot(Right_TopCorner(1),Right_TopCorner(2),'c.','MarkerSize',20);
        plot(Right_BtmCorner(1),Right_BtmCorner(2),'r.','MarkerSize',20);
        plot(Left_TopCoord(1),Left_TopCoord(2),'g.','MarkerSize',20);       
    end

%changes the value of x and y pixel coordinates according to selected
%reference point
    function Menu_Callback(hObject, event)
    global Bottom_LeftCoord Right_TopCorner Right_BtmCorner Left_TopCoord;
    str = fcmenu.String;
    val = fcmenu.Value;
        switch str{val};
            case 'Bottom-Left'
                    set(xPx, 'String',Bottom_LeftCoord(1))
                    set(yPx, 'String',Bottom_LeftCoord(2))
            case 'Top-Left'
                    set(xPx, 'String',Left_TopCoord(1))
                    set(yPx, 'String',Left_TopCoord(2))
            case 'Top-Right'
                    set(xPx, 'String',Right_TopCorner(1))
                    set(yPx, 'String',Right_TopCorner(2))
            case 'Bottom-Right'
                    set(xPx, 'String',Right_BtmCorner(1))
                    set(yPx, 'String',Right_BtmCorner(2))
        end
    end

%Stores all the info into a reference frame will can be assessed later
    function store_Callback(hObject, event)
 
    data = guidata(hObject);
    str = fcmenu.String;
    val = fcmenu.Value;
    num = 0;
    switch str{val};
        case 'Bottom-Left'
                num = 1;
                A = 'BL';
        case 'Top-Left'
                num = 2;
                A = 'TL';
        case 'Top-Right'
                num = 3;
                A = 'TR';
        case 'Bottom-Right'
                num = 4;
                A = 'BR';
    end
    
    lagno = str2double(get(fclat, 'String'));
    lonno = str2double(get(fclon, 'String'));
    xPxcoord = str2double(get(xPx, 'String'));
    yPxcoord = str2double(get(yPx, 'String'));
   
    frame.(A).LON= lonno;
    frame.(A).LAT = lagno;
    frame.(A).XP = xPxcoord;
    frame.(A).YP = yPxcoord; 
    number = get(BlockNo, 'String');
    guidata(hObject, data);
    userPrompt = sprintf('Stored');
	reply = questdlg(userPrompt, 'Stored', 'Ok', 'Stored');
    set(fclat, 'String', '');
    set(fclon, 'String','');
    save(['reference_frameblock' number], 'frame');

    end 

    function startrow_Callback(hObject, event)
      data = guidata(hObject);
      %find the operator figure
      cobj = findobj('Tag', 'operator');
      figure (cobj);
      %manual click to get start point and the 2nd point is 100 px away in
      %the x direction
      start = ginput(1);
      start2 = [start(1) + 100, start(2)];
      startRow = [start ; start2];
      data.startRow = startRow;
      guidata(hObject, data);
    end


end

