function [RGB] = DefectOnBluePrint(position, coord, altitude, I, refFrame, offset)
%plot a box of the defect on the blueprint
%Inputs:
%   position - a matrix of POI of [X Y], must contain at least 4 corner points
%   coordOfDefect - GPS coordinate of the corresponding picture that has
%       defect [Lat Lon]
%   altitude - flying altitude of the drone
%   I - blueprint image
%   refFrame - reference picture or the blueprint
%   offset - [x y] values positive x shifts to the right, positive y shift
%       to the bottom
%

    RGB = I;
    
    % extract reference image points at 4 corners
    TR = refFrame.TR;
    TL = refFrame.TL;
    BR = refFrame.BR;
    BL = refFrame.BL;
    
    %convert the corners into pixels and corresponding coordinates
    PixelX = [TR.XP TL.XP BR.XP BL.XP];
    PixelY = [TR.YP TL.YP BR.YP BL.YP];
    Lat = [TR.LAT TL.LAT BR.LAT BL.LAT];
    Lon = [TR.LON TL.LON BR.LON BL.LON];
    
    %locate the defect picture coordinate on the blueprint
    F_PixelX = scatteredInterpolant(Lat',Lon',PixelX');
    F_PixelY = scatteredInterpolant(Lat',Lon',PixelY');
    
    Pix_X = F_PixelX(coord(1),coord(2));
    Pix_Y = F_PixelY(coord(1),coord(2));
    
    %Field of view of the thermal camera
    FOV_H = 32; %horizontal
    FOV_V = 26; %vertical
    
    %Pixels number of the picture captured
    Px = 640; %horizontal
    Py = 512; %vertical
    
    %center pixel of the image
    Px_mid = Px/2;
    Py_mid = Py/2;
    
    %distance per pixel
    Dist_Pix_X = abs((2*altitude*tan(deg2rad(FOV_H)/2))/Px); %for image
    Dist_Pix_Y = abs((2*altitude*tan(deg2rad(FOV_V)/2))/Py);
    Dist_Pix_BPx = fngps2dist2px(refFrame); %for blueprint
    Dist_Pix_BPy = fngps2dist2py(refFrame);
    
    %pixel coodinate of the POI
    x = position(:,1);
    y = position(:,2);
    
    %distance from the center of the image (in meter)
    dx = (x-ones(size(x))*Px_mid)*Dist_Pix_X;
    dy = (y-ones(size(y))*Py_mid)*Dist_Pix_Y;
    
    %relative pixel on the blueprint (in pixel)
    DX = abs(dx./Dist_Pix_BPx + ones(size(x))*(Pix_X+offset(1)/Dist_Pix_BPx));
    DY = abs(dy./Dist_Pix_BPy + ones(size(y))*(Pix_Y+offset(2)/Dist_Pix_BPy));
    
    %check if there is defect in the data
    m = length(DX);
    for j=5:m
        RGB = insertMarker(RGB, [DX(j) DY(j)]);
    end
    %arrange them into an array for insertObject position
    P = [DX(1) DY(1) DX(2) DY(2) DX(3) DY(3) DX(4) DY(4)]
    
    RGB = insertShape(RGB,'polygon',P, 'LineWidth', 5);
    
end