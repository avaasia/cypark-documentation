function [ alarms ] = findAlarms( imgInfo, refFrame )
    
    alarms = struct;
    dist_missing = find(imgInfo.DIST > 8);
    time_missing = find(imgInfo.TIMEELAPSED > 6);
    
    lats = imgInfo.LAT;
    lons = imgInfo.LON;
    cameraPitch = imgInfo.CP;
    nooffiles = length(lons);
    
    minLon = min([refFrame.TL.LON, refFrame.TR.LON, refFrame.BL.LON, refFrame.BR.LON]);
    maxLon = max([refFrame.TL.LON, refFrame.TR.LON, refFrame.BL.LON, refFrame.BR.LON]);
    
    minLat = min([refFrame.TL.LAT, refFrame.TR.LAT, refFrame.BL.LAT, refFrame.BR.LAT]);
    maxLat = max([refFrame.TL.LAT, refFrame.TR.LAT, refFrame.BL.LAT, refFrame.BR.LAT]);
    
    a = find(lons < minLon | lons > maxLon);
    b = find(lats < minLat | lats > maxLat);
    c = find(cameraPitch > -88.5 | cameraPitch < -91.5);
    
    alarms.DISTALARM = dist_missing;
    alarms.TIMEALARM = time_missing;
    alarms.LONALARM = a;
    alarms.LATALARM = b;
    alarms.ANGALARM = c;
end

