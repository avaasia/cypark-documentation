function [x_waypoint,y_waypoint]  =  fnLawnmowerSearch(x_grid,y_grid,searchType)
% Lawnmower type area search algorithm
% x_grid = x-coordinate of grids to visit
% y_grid = y-coordiate of grids to visit
% searchType = column or row type
%
% sample usage: [x_waypoint,y_waypoint] =
% fnLawnmowerSearch(x_grid,y_grid,'row');

if strcmp(searchType,'row')
    % lawnmower type search (row by row)
    [y_waypoint,idx] = sort(y_grid,'descend');
    x_waypoint = x_grid(idx);
    [~,idx_y] = unique(y_waypoint);
    idx_y = flipud(idx_y);
    % sort the next row in a reverse order
    for i=2:2:length(idx_y)-1
        x_waypoint(idx_y(i):idx_y(i+1)-1)=sort(x_waypoint(idx_y(i):idx_y(i+1)-1),'descend');
    end
    
elseif strcmp(searchType,'column')
    % lawnmower type search (column by column)
    [y_waypoint,idx] = sort(y_grid,'descend');
    x_waypoint = x_grid(idx);
    [x_waypoint,idx] = sort(x_waypoint,'ascend');
    y_waypoint = y_waypoint(idx);
    [~,idx_x] = unique(x_waypoint);
    % sort the next column in a reverse order
    for i=2:2:length(idx_x)-1
        y_waypoint(idx_x(i):idx_x(i+1))=sort(y_waypoint(idx_x(i):idx_x(i+1)),'ascend');
    end
    
else
    error('Unrecognized searchType for lawnmover search');
end
end