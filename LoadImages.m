function [RBG] = LoadImages( counter, filename, RBG, frame, info)
%counter from handles.counter to monitor when user click next or prev
%filename contains the list of images in Images directory
%RBG refers to the blueprint
%frame is the reference frame

    img = imfinfo(filename{counter});
    gps = img.GPSInfo;
    lat = gps.GPSLatitude(1) + (gps.GPSLatitude(2)/60) + (gps.GPSLatitude(3)/3600);
    lon = gps.GPSLongitude(1) + (gps.GPSLongitude(2)/60) + (gps.GPSLongitude(3)/3600);
    
    yaw = info.CY(counter)
    alt = info.alt(counter)
    
    I = imread(filename{counter});
    %transform the picture to yaw 0 degree
    [P,B] = TransformImage(I, yaw, 0);
    imshow(B);
    
    RBG = imread(RBG);
    %map the transformed picture onto the blueprint
    RBG = DefectOnBluePrint(P,[lat lon], alt, RBG, frame, [-0.8 -2]); %added offset
    

end

