function [ rad ] = fndeg2rad( deg )
%Convert degree to rad
    rad = deg * pi/180;
end

