function [ im_converted ] = convertImages( start_num, imageInfo, refFrame)
%This function finds the pixel locations of the images on the reference
%picture.
%Inputs:
%   start_num (integer): starting number of the image in the image set that need to
%       perform the search
%   imageInfo (struct): contains info of the images of interest
%   refFrame (struct): reference picture
%Output:
%   im_converted (struct): contains pixel coordinates of the images of
%       interest
%

    % extract reference image points at 4 corners
    TR = refFrame.TR;
    TL = refFrame.TL;
    BR = refFrame.BR;
    BL = refFrame.BL;
    
    %convert the corners into pixels and corresponding coordinates
    PixelX = [TR.XP TL.XP BR.XP BL.XP];
    PixelY = [TR.YP TL.YP BR.YP BL.YP];
    Lat = [TR.LAT TL.LAT BR.LAT BL.LAT];
    Lon = [TR.LON TL.LON BR.LON BL.LON];
    
    %info for the input images that need to match to the reference image
    longitudes = imageInfo.LON;
    latitudes = imageInfo.LAT;
    altitudes = imageInfo.ALTABS;
    nooffiles = length(longitudes);
    
    %create return parameter and empty coordinates
    im_converted = struct;
    xcoor = zeros(nooffiles, 1);
    ycoor = zeros(nooffiles, 1);
    
    %equation to take altitude into account?
    %%%%%%% NEED CLARIFICATION %%%%%%%
    %mx = -0.2768;
    %Kx = 9.7723;
    
    %my = 1.5589;
    %Ky = -33.026;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %functions to find pixel location for the POI
    F_PixelX = scatteredInterpolant(Lat',Lon',PixelX');
    F_PixelY = scatteredInterpolant(Lat',Lon',PixelY');
    
    for i = start_num:nooffiles
        img_lon = longitudes(i);
        img_lat = latitudes(i);
        img_alt = altitudes(i);

%%%FROM OLD CODES%%%
%         syms x y
%         eqn1 = C1*x + C3*y == (img_lon - refFrame.BL.LON);
%         eqn2 = C2*x + C4*y == (img_lat - refFrame.BL.LAT);
%         
%         [E1, E2] = equationsToMatrix([eqn1, eqn2], [x, y]);
%         p_c = linsolve(E1, E2);
%         Px = refFrame.BL.XP + double(p_c(1));
%         Py = refFrame.BL.YP + double(p_c(2));
        
        %NEW SOLVING METHOD
        Px = F_PixelX(img_lat,img_lon);
        Py = F_PixelY(img_lat,img_lon);
        
        %equation to convert altitude?
        %Px = Px - mx*img_alt - Kx;
        %Py = Py - my*img_alt - Ky;
        
        %final coordinate
        xcoor(i) = Px;
        ycoor(i) = Py;
    end
    
    %return parameter
    im_converted.X = xcoor(start_num:nooffiles);
    im_converted.Y = ycoor(start_num:nooffiles);
end

