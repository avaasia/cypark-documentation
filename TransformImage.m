function [P, B] = TransformImage(I, TrueNorth, offset, bbox)
%function to transform the image that contains defect and return the pixel
%coordinate of the 4 corners and the defect after the transformation
%Input:
%   I - image that contains defect to transform
%   TrueNorth (degree) - yaw angle of the gimbal 
%   offset - degree offset to add to the rotation, positive means CCW
%   bbox (optional) - bbox object that contain the top left coordinate of the boundary
%       box
% Output:
%   P - matrix that contains the four corners and the defect after the
%       tranformation. Defect coordinate is store at the last row of P.
%   B - transformed image
    
    if ~exist('bbox','var')
        bbox = 0;
    end
    
    %get the image size
    [height, width, dim] = size(I);
    
    %build transformation matrix
    Rin = imref2d(size(I));
    %Rin.XWorldLimits = Rin.XWorldLimits - mean(Rin.XWorldLimits); %make the rotation reference at the center of image
    %Rin.YWorldLimits = Rin.YWorldLimits - mean(Rin.YWorldLimits);
    tX = mean(Rin.XWorldLimits);
    tY = mean(Rin.YWorldLimits);
    
    %translation origin to the center of image
    tTranslationToCenter = [1 0 0; 0 1 0; -tX -tY 1];
    %rotation matrix with angle from true north
    %positive angle means anti-clockwise rotation
    TrueNorth = TrueNorth + offset;
    tRotation = [cosd(TrueNorth) -sind(TrueNorth) 0; sind(TrueNorth) cosd(TrueNorth) 0; 0 0 1];
    %translation the origin back to the original origin
    tTranslationBack = [1 0 0; 0 1 0; tX tY 1];
    %transformation matrix to rotate the picture at the center of the picture
    tform = tTranslationToCenter*tRotation*tTranslationBack;
    %matrix format accepted by imwarp
    tform = affine2d(tform);
    %performa the transform
    [B, ref] = imwarp(I, tform);
    
    %get the points at the transformed image
    %not the exact corners but 10 pixels away because image may be cropped
    %due to the transformation
    if bbox==0
        u = [10 10 width-10 width-10];
        v = [10 height-10 height-10 10];
    else
        u = [10 10 width-10 width-10 bbox(1)];
        v = [10 height-10 height-10 10 bbox(2)];
    end
    [x, y] = transformPointsForward(tform, u, v);
    x = x - ref.XWorldLimits(1);
    y = y - ref.YWorldLimits(1);
    P = [x;y]';
    
end