function [ blueprint ] = fnAsBuiltExtraction( I )
%Extract the blueprint and do image processing analysis
%   1) Convert to gray
%   2) Threshold gray scale
%   3) Fill up the holes
%   4) Dilate the image to form big area
%   5) Output corners of the block of the blueprint
%   6) Integrate to show solar panel

% Convert to grayscale image in double
I_gray = im2double(rgb2gray(I));

% Extract edges of the blueprint
edge = I_gray <0.7;

% Pajam Plant
SE = strel('square',3);
edge = imdilate(edge,SE);


% Fill up the holes
I_fill = imfill(edge, 'holes');


% Dilate the image to form one block only
SE = strel('rectangle', [20 10]);
I_dilate = imdilate(I_fill,SE);
%I_erode = imerode(I_dilate, strel('rectangle', [34 17]));
%imshow(I_erode);

% Identify the corners of the block
corners = corner(I_dilate);

% Combine with the solar panels
I_final = I_dilate>0 & edge<1;

blueprint = struct('bluePrint',I_final,'boundingArea',I_dilate);
%figure;
%imshow(blueprint.bluePrint)
end

