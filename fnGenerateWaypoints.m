function fnGenerateWaypoints (boundaryPoints, bearing_line, fwdDist, sideDist,panelWidth,overshoot)

%boundary lat lon in deg

%{
%side dist 
sideDist = 5;
fwdDist = 10;
%}

%treat first point as (0,0). find the relative x,y distances of other points
pfix = zeros(length(boundaryPoints), 2);
neg =1;
for i = 2:length(boundaryPoints)
   if boundaryPoints(i,1)<boundaryPoints(1,1)
       neg=-1;
   else 
       neg=1;
   end
   pfix(i,1) = neg*1000*pos2dist(boundaryPoints(1,1),boundaryPoints(1,2),boundaryPoints(i,1),boundaryPoints(1,2),1);
   if boundaryPoints(i,2)<boundaryPoints(1,2)
       neg=-1;
   else 
       neg=1;
   end
   pfix(i,2) = neg*1000*pos2dist(boundaryPoints(1,1),boundaryPoints(1,2),boundaryPoints(1,1),boundaryPoints(i,2),1);
end

% flip(y,x) to (x,y), because lat is y lon is x
pfix = fliplr(pfix);

%account for bearing, rotate around start point
bearing = getBearing(bearing_line(1,1),bearing_line(1,2),bearing_line(2,1),bearing_line(2,2));
R=[cos(-bearing) (-sin (-bearing)); sin(-bearing) cos(-bearing)];
Rinv=[cos(bearing) (-sin (bearing)); sin(bearing) cos(bearing)];
pfix=pfix*R;

%figure, plot(pfix2(:,1), pfix2(:,2))


% edge = size of the individual mesh
% aircraft ground speed in m/s
speed = 5;

% generate grid
figure;
[x_grid,y_grid]  = fnSquareGrid(pfix, fwdDist, sideDist,panelWidth, overshoot);

% Generate way point
% can change the third input to 'column' as seem fit to the boundary region
[x_waypoint,y_waypoint] = fnLawnmowerSearch(x_grid,y_grid,'row');
A=[x_waypoint,y_waypoint] *Rinv;




% the result
  plot(pfix(:,1),pfix(:,2),'r','linewidth',2);
  line(x_waypoint,y_waypoint);
  line(x_grid,y_grid,'marker','.','linestyle','none');
  axis equal;
  ylabel('m');
  xlabel('m');
  title('Flight Area');
  grid;
  
  
%convert planar dist back to coordinates
%lat_dist=y_waypoint;
%lon_dist=x_waypoint;

lat_dist=A(:,2);
lon_dist=A(:,1);

coordinates = zeros(length(lat_dist), 2);

neg =1;
for i = 1:length(lat_dist)
   %initial coordinate plus relative distance
   %add lat dist
   [templat,templon]= posAddDist(boundaryPoints(1,1),boundaryPoints(1,2),lat_dist(i),0);
   %add lon_dist
   [coordinates(i,1),coordinates(i,2)]=posAddDist(templat,templon,lon_dist(i),90);
end

% the result
  plot(boundaryPoints(:,1),boundaryPoints(:,2),'r','linewidth',2);
  line(coordinates(:,1),coordinates(:,2));
  line(coordinates(:,1),coordinates(:,2),'marker','.','linestyle','none');
  axis equal;
  ylabel('lon');
  xlabel('lat');
  title('Coordinates');
  grid;

%calculate total distance travelled
distance = 0;
for i=2:length(x_waypoint)
    distance = distance + sqrt((x_waypoint(i)-x_waypoint(i-1))^2+...
                                (y_waypoint(i)-y_waypoint(i-1))^2);
end

dlmwrite('waypoints3.csv',coordinates,'precision','%.6f','delimiter',',');

distance
flightTime = distance/speed/60