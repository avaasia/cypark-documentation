function [ dist2px ] = fngps2dist2px( refPts )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

TR = refPts.TR;
TL = refPts.TL;
BR = refPts.BR;
BL = refPts.BL;
 
% X direction (column-wise)
pixel_diff_XT = TR.XP - TL.XP;
pixel_diff_XB = TR.XP - TL.XP;
pixel_diff_X = mean([pixel_diff_XT pixel_diff_XB]);

% Y direction (row-wise)

pixel_diff_YL = TL.YP - BL.YP;
pixel_diff_YR = TR.YP - BR.YP;
pixel_diff_Y = mean([pixel_diff_YL pixel_diff_YR]);

dist2px = mean([pixel_diff_X pixel_diff_Y]);

end

