function rad_bearing = getBearing(deg_start_lat1,deg_start_lon1,deg_end_lat2,deg_end_lon2)
%? = atan2( sin ?? ? cos ?2 , cos ?1 ? sin ?2 ? sin ?1 ? cos ?2 ? cos ?? )
%where	?1,?1 is the start point, ?2,?2 the end point (?? is the difference in longitude)
rad_lat1=deg_start_lat1*pi/180;
rad_lon1=deg_start_lon1*pi/180;
rad_lat2=deg_end_lat2*pi/180;
rad_lon2=deg_end_lon2*pi/180;
diff_lon=rad_lon2-rad_lon1;

rad_bearing = atan2(sin(diff_lon)*cos(rad_lat2),cos(rad_lat1)*sin(rad_lat2)-sin(rad_lat1)*cos(rad_lat2)*cos(diff_lon));


