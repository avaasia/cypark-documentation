function [x_grid,y_grid]  = fnPxSquareGrid(startRow, boundaryArea, fwdPx, sidePx)
% define flight boundary and discretize area
% generate square grid
% startRow is parallel to horizon. (x1,y),(x2,y)


%if (size(startRow)~=4)
% expect 2X2 matrix for startRow
%if (startRow(1,2)~=startRow(2,2)
%throw error expect startRow to be horizontal (y1==y2)
% startRow is in boundaryArea. How to check?
%  ip=inpolygon(xd,yd,xt,yt);
xt = boundaryArea (:,1);
yt = boundaryArea (:,2);
    

startY=startRow(1,2);
minYArea=min(boundaryArea(:,2));
maxYArea=max(boundaryArea(:,2));
if (abs(startY-minYArea)>abs(startY-maxYArea))
    endY=minYArea;
    %startY > endY
    xp = min(xt):fwdPx:max(xt);
    yp = startY:-sidePx:endY;

else 
    endY=maxYArea;
    %startY < endY
    xp = min(xt):fwdPx:max(xt);
    yp = startY:sidePx:endY;
end

 [xd,yd]=meshgrid(xp,yp);
 
 ip=inpolygon(xd,yd,xt,yt);
 x_grid = xd(ip);
 y_grid = yd(ip);
 %{
 xp = min(pfix(:,1))+fwdDist/2:fwdDist:max(pfix(:,1));
 yp = min(pfix(:,2))+sideDist/2:sideDist:max(pfix(:,2));
 [xd,yd]=meshgrid(xp,yp);
 
 
 ip=inpolygon(xd,yd,pfix(:,1),pfix(:,2));
 x_grid = xd(ip);
 y_grid = yd(ip);
 %}
end
