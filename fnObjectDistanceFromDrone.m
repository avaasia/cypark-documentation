function [ Alt ] = fnObjectDistanceFromDrone( dist_px,H_FOV )
%Find the distance between the object in the image to the drone
    Alt = dist_px*320/tan(deg2rad(H_FOV)/2);
    %Alt = dist_px*320/tan(0.558505/2);
end

