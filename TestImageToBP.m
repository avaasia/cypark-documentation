
%test plotting box of defect on the blueprint
clc; clear; close all;

load reference_frame5.mat;
load bbox.mat;
%get all the images
%cd('Images');
files = dir('*.JPG');
numFile = numel(files);
%blueprint
RBG = imread('Block2.png');

%run the code
for i=1:1
    img = imfinfo(files(i).name)
    gps = img.GPSInfo;
    lat = gps.GPSLatitude(1) + (gps.GPSLatitude(2)/60) + (gps.GPSLatitude(3)/3600);
    lon = gps.GPSLongitude(1) + (gps.GPSLongitude(2)/60) + (gps.GPSLongitude(3)/3600);
    
    %call exiftool
    [status, cmdout] = system(['exiftool ' files(i).name]);
        
    if status ~= 0
        disp('Error in extracting image information');
        break
    end
        
    cmds = strsplit(cmdout, '\n');
    % Camera yaw
    yaw = strsplit(cmds{37}, ': ');
    yaw = str2double(yaw{2});
    alt = strsplit(cmds{35}, ': ');
    alt = str2double(alt{2});
    
    I = imread(files(i).name);
    %transform the picture to yaw 0 degree
    [P,B] = TransformImage(I, yaw, 0);
    %map the transformed picture onto the blueprint
    RBG = DefectOnBluePrint(P,[lat lon], alt, RBG, frame, [-0.8 -2]); %added offset
end

figure(1); imshow(RBG);
% I1 = imread('DJI_0705.jpg');
% [P1,B1] = TransformImage(I1, -179.5, 0, bbox(2,:));
% figure(1);
% imshowpair(I1,B1);
% 
% I2 = imread('DJI_0706.jpg');
% [P2,B2] = TransformImage(I2, -172.3, 0);
% figure(2);
% subplot(1,2,1); imshow(I2);
% subplot(1,2,2); imshow(B2);
% 
% I3 = imread('DJI_0707.jpg');
% [P3,B3] = TransformImage(I3, -172.3, 0);
% figure(3);
% subplot(1,2,1); imshow(I3);
% subplot(1,2,2); imshow(B3);
% 
% I4 = imread('DJI_0734.jpg');
% [P4,B4] = TransformImage(I4, 7.3, 0);
% figure(4);
% subplot(1,2,1); imshow(I4);
% subplot(1,2,2); imshow(B4);
% 
% I5 = imread('DJI_0732.jpg');
% [P5,B5] = TransformImage(I5, 7.7, 0);
% figure(5);
% subplot(1,2,1); imshow(I5);
% subplot(1,2,2); imshow(B5);
% 
% I6 = imread('DJI_0744.jpg');
% [P6,B6] = TransformImage(I6, -0.7, 0);
% figure(6);
% subplot(1,2,1); imshow(I6);
% subplot(1,2,2); imshow(B6);
% 
% I7 = imread('DJI_0755.jpg');
% [P7,B7] = TransformImage(I7, 179, 0);
% figure(7);
% subplot(1,2,1); imshow(I7);
% subplot(1,2,2); imshow(B7);
% 
% I8 = imread('DJI_0760.jpg');
% [P8,B8] = TransformImage(I8, 179.1, 0);
% figure(8);
% subplot(1,2,1); imshow(I8);
% subplot(1,2,2); imshow(B8);
% 
% IBP = imread('Block2.png');
% 
% % figure(9);
% % subplot(2,2,1); imshow(B2);
% % subplot(2,2,2); imshow(B3);
% % subplot(2,2,3); imshow(B4);
% % subplot(2,2,4); imshow(B5);
% % RGB = insertShape(I2,'polygon',P2);
% % figure(2)
% % imshow(RGB);
% RBG = DefectOnBluePrint(P1,[2.8350634 101.8469881], 16.8, IBP, frame, [-0.8 -2]);
% RBG = DefectOnBluePrint(P2,[2.83506411 101.84705975], 17.2, RBG, frame, [-0.8 -2]);
% RBG = DefectOnBluePrint(P3,[2.83506422 101.84713153], 17.3, RBG, frame, [-0.8 -2]);
% RBG = DefectOnBluePrint(P4,[2.83522639 101.84719347], 16.5, RBG, frame, [-0.8 -2]);
% RBG = DefectOnBluePrint(P5,[2.835227972 101.847355056], 16.5, RBG, frame, [-0.8 -2]);
% RBG = DefectOnBluePrint(P6,[2.835251028 101.84733425], 16.8, RBG, frame, [-0.8 -2]);
% RBG = DefectOnBluePrint(P7,[2.835304639 101.847148556], 16.6, RBG, frame, [-0.8 -2]);
% RBG = DefectOnBluePrint(P8,[2.835304667 101.847509167], 16.3, RBG, frame, [-0.8 -2]);
% figure(10)
% imshow(RBG);
%angle = FindBPTrueNorth(frame)