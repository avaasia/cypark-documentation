function [  ] = fnReferenceFramePlotting( r )
%Plot the reference frame 4 corners
        plot(r.TL.XP, r.TL.YP, 'g.', 'MarkerSize', 20);
        plot(r.TR.XP, r.TR.YP, 'c.', 'MarkerSize', 20);
        plot(r.BL.XP, r.BL.YP, 'b.', 'MarkerSize', 20);
        plot(r.BR.XP, r.BR.YP, 'r.', 'MarkerSize', 20);

end

