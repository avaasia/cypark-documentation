function [ dist2px ] = fngps2dist2px( refPts )
%Compute the distance in m per px of a given points in a given blueprint
%   Detailed explanation goes here

TR = refPts.TR;
TL = refPts.TL;
BR = refPts.BR;
BL = refPts.BL;

% Pixel X direction (column-wise)
pixel_diff_XT = TR.XP - TL.XP;
pixel_diff_XB = BR.XP - BL.XP;

% Pixel Y direction (row-wise)
pixel_diff_YT = TR.YP - TL.YP;
pixel_diff_YB = BR.YP - BL.YP;

% Angle difference
angle_T = atan(pixel_diff_YT/pixel_diff_XT);
angle_B = atan(pixel_diff_YB/pixel_diff_XB);

% Total Distance Calculation
dist_diff_T = pos2dist(TR.LAT,TR.LON,TL.LAT,TL.LON,2)*1000;
dist_diff_B = pos2dist(BR.LAT,BR.LON,BL.LAT,BL.LON,2)*1000;

% Total Distance in X direction
dist_diff_XT = dist_diff_T * cos(angle_T);
dist_diff_XB = dist_diff_B * cos(angle_B);

% Total Distance in Y direction
dist_diff_YT = dist_diff_T * sin(angle_T);
dist_diff_YB = dist_diff_B * sin(angle_B);

% Distance per px
distppx_T = dist_diff_XT/pixel_diff_XT;
distppx_B = dist_diff_XB/pixel_diff_XB;

dist2px = mean([distppx_T distppx_B]);

end

