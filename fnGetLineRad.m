function theta = fnGetLineRad (line)
% {{x1,y1}
%  {x2,y2}}

dy=line(1,2)-line(2,2);
dx=line(1,1)-line(2,1);

theta = atan(dy/dx);

