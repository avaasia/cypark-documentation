% pixel_of_picture = (GPS_of_picture - GPS_of_reference)/GPS_per_pixel

function [SP] = CalculateSpan( refPts )

TR = refPts.TR;
TL = refPts.TL;
BR = refPts.BR;
BL = refPts.BL;

% Calculate the difference in pixels between each corner
% Calculate how much the lon and lat changes for each pixel in either direction

% X direction (column-wise)
pixel_diff_XT = TR.XP - TL.XP;
lon_span_XT = (TR.LON - TL.LON)/pixel_diff_XT;
lat_span_XT = (TR.LAT - TL.LAT)/pixel_diff_XT;

pixel_diff_XB = BR.XP - BL.XP;
lon_span_XB = (BR.LON - BL.LON)/pixel_diff_XB;
lat_span_XB = (BR.LAT - BL.LAT)/pixel_diff_XB;

% average out for better accuracy 
XLON = mean([lon_span_XT, lon_span_XB]);
XLAT = mean([lat_span_XT, lat_span_XB]);

% Y direction (row-wise)
pixel_diff_YL = TL.YP - BL.YP;
lon_span_YL = (TL.LON - BL.LON)/pixel_diff_YL;
lat_span_YL = (TL.LAT - BL.LAT)/pixel_diff_YL;

pixel_diff_YR = TR.YP - BR.YP;
lon_span_YR = (TR.LON - BR.LON)/pixel_diff_YR;
lat_span_YR = (TR.LAT - BR.LAT)/pixel_diff_YR;

% average out for better accuracy
YLON = mean([lon_span_YL, lon_span_YR]);
YLAT = mean([lat_span_YL, lat_span_YR]);

% Store and return all the values > these will be the coefficients for
% solving simultaneous equations later
SP = struct('XLON', XLON, 'XLAT', XLAT, 'YLON', YLON, 'YLAT', YLAT);
end

