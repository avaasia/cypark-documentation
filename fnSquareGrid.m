function [x_grid,y_grid]  = fnSquareGrid(pfix, fwdDist, sideDist,panelWidth, overshoot)
% define flight boundary and discretize area
% overshoot is the x overshoot in the x direction eg. overshoot=1.2;
% generate square grid
 x=pfix(:,1);
 y=pfix(:,2);
 
 %scale across the panel by overshoot %
 xt = overshoot*x+(1-overshoot)*mean(x(1:end-1));
 yt = y;
 
 
 xp = min(xt)-fwdDist/2:fwdDist:max(xt);
 yp = min(yt)+panelWidth/2:sideDist:max(yt);
 [xd,yd]=meshgrid(xp,yp);
 
  ip=inpolygon(xd,yd,xt,yt);
 x_grid = xd(ip);
 y_grid = yd(ip);
 %{
 xp = min(pfix(:,1))+fwdDist/2:fwdDist:max(pfix(:,1));
 yp = min(pfix(:,2))+sideDist/2:sideDist:max(pfix(:,2));
 [xd,yd]=meshgrid(xp,yp);
 
 
 ip=inpolygon(xd,yd,pfix(:,1),pfix(:,2));
 x_grid = xd(ip);
 y_grid = yd(ip);
 %}
end
