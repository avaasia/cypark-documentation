function waypoints=fnGeneratePxWaypoints (boundaryArea, startRow, fwdDist, sideDist,dist2PxRatio)

%boundaryPoints is the area of no fly zone. px coordinates of polygon
%startRow is a 2x2 matrix. consist of 2 points (x,y) in pixel units
%fwdDist is the a user input in m, it is the resolution of each grid point
%generated. 
%sideDist is a user input in m. Dist between 2 solar panel rows)
%dist2PxRatio is px/dist. convert dist (m) to px of the blueprint 

%account for bearing, rotate around origin
rowAngle = fnGetLineRad (startRow)

%rotation matrix around origin
R=[cos(rowAngle) (-sin (rowAngle)); sin(rowAngle) cos(rowAngle)]
Rinv=[cos(-rowAngle) (-sin (-rowAngle)); sin(-rowAngle) cos(-rowAngle)]

%rotate startRow and boundaryArea such that startRow is parallel to horizon
rBoundaryArea = boundaryArea*R;
rStartRow=startRow*R;

  %plot(rBoundaryArea(:,1),rBoundaryArea(:,2),'b','linewidth',1);
    
  line(rStartRow(:,1),rStartRow(:,2),'marker','.','color','r');
  line(boundaryArea(:,1),boundaryArea(:,2),'Color','g');
  line(startRow(:,1),startRow(:,2),'marker','.','Color','g');
  set(gca,'YDir','Reverse')
  axis equal;
  ylabel('px');
  xlabel('px');
  title('px rotation');
  grid;

  
  fwdPx = fwdDist*dist2PxRatio;
  sidePx = sideDist*dist2PxRatio;
% generate grid waypoints
[rX_grid,rY_grid]  = fnPxSquareGrid(rStartRow, rBoundaryArea, fwdPx, sidePx);

% Generate flight path using waypoints
%get start and end point of each row
[rX_waypoint,rY_waypoint]=fnMinMaxRow( rX_grid,rY_grid);


% can change the third input to 'column' as seem fit to the boundary region
%[rX_waypoint,rY_waypoint] = fnLawnmowerSearch(rX_grid,rY_grid,'row');
%line(rX_waypoint,rY_waypoint,'Color','b');
waypoints=[rX_waypoint,rY_waypoint] *Rinv;
%grids=[rX_grid,rY_grid] *Rinv;
line(waypoints(:,1),waypoints(:,2),'Color','r');
%line(grids(:,1),grids(:,2),'marker','.','linestyle','none','Color','g');
line(waypoints(:,1),waypoints(:,2),'marker','.','linestyle','none','Color','r');

