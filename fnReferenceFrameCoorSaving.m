function [ r ] = fnReferenceFrameCoorSaving( r, ref_coor )
%Assign the reference_frame with correct coordinate in the picture
% T = Top, B = Bottom, L = Left, R = Right, XP = X Position
        
        r.BL.XP = ref_coor(1,1);
        r.BL.YP = ref_coor(1,2);
        r.TL.XP = ref_coor(2,1);
        r.TL.YP = ref_coor(2,2);
        r.TR.XP = ref_coor(3,1);
        r.TR.YP = ref_coor(3,2);
        r.BR.XP = ref_coor(4,1);
        r.BR.YP = ref_coor(4,2);

end



