function [ ReferenceCoor ] = fnReferenceCoordinateFinding( regProp )
% Find the 4 reference corners of the blueprint
        
        %find which rows has the smallest x coordinate
        rowx= find(regProp.Centroids==min(regProp.Centroids(:,1)));
        
        %Find the corresponding y coordinate
        MinVal_Y=regProp.Centroids(rowx(1),2); 
        for x1 = 1:size(rowx,1)
            if regProp.Centroids(rowx(x1),2)<MinVal_Y; 
            MinVal_Y=regProp.Centroids(rowx(x1),2); 
            end
        end
        Left_TopCent = [min(regProp.Centroids(:,1)), MinVal_Y];
        indexLT = find(ismember(regProp.Centroids,Left_TopCent,'rows'));
        Left_TopCoord = [regProp.BoundingBox(indexLT),regProp.BoundingBox(indexLT,2)];
        
        %finding the top right corner coordinates
        rowTop= find(regProp.BoundingBox(indexLT,2)== regProp.BoundingBox(:,2));
        MaxVal_X = regProp.BoundingBox(indexLT);
         for x2 = 1:size(rowTop,1)
            if regProp.BoundingBox(rowTop(x2),1)> MaxVal_X; 
            MaxVal_X=regProp.BoundingBox(rowTop(x2),1); 
            end
        end
        Right_TopCorner = [MaxVal_X + regProp.BoundingBox(rowTop(x2),3), regProp.BoundingBox(indexLT,2)];
        
        %finding the bottom left corner coordinates
        rowy= find(regProp.Centroids(:,2)==max(regProp.Centroids(:,2)));
        MinVal_X=regProp.Centroids(rowy(1),1); 
        for x3 = 1:size(rowy,1)
            if regProp.Centroids(rowy(x3),1)<MinVal_X; 
            MinVal_X=regProp.Centroids(rowy(x3),1); 
            end
        end
        Bottom_LeftCent = [MinVal_X, max(regProp.Centroids(:,2))];
        indexBL = find(ismember(regProp.Centroids,Bottom_LeftCent,'rows'));
        Bottom_LeftCoord = [regProp.BoundingBox(indexBL), ...
            (regProp.BoundingBox(indexBL,2)+regProp.BoundingBox(indexBL,4))];
        
        %finding the bottom right corner coordinates
        rowBtm= find(regProp.BoundingBox(indexBL,2)== regProp.BoundingBox(:,2));
        MaxVal_X2 = regProp.BoundingBox(indexBL);
         for x4 = 1:size(rowBtm,1)
            if regProp.BoundingBox(rowBtm(x4),1)> MaxVal_X2; 
            MaxVal_X2=regProp.BoundingBox(rowBtm(x4),1); 
            end
        end
        Right_BtmCorner = [MaxVal_X2 + regProp.BoundingBox(rowBtm(x4),3), ... 
            (regProp.BoundingBox(indexBL,2)+regProp.BoundingBox(indexBL,4))];
        
        ReferenceCoor = [Bottom_LeftCoord(1,1) Bottom_LeftCoord(1,2);
            Left_TopCoord(1,1) Left_TopCoord(1,2);
            Right_TopCorner(1,1) Right_TopCorner(1,2);
            Right_BtmCorner(1,1) Right_BtmCorner(1,2)]


end

