function [ deg ] = fnrad2deg( rad )
%Convert radian to degree
    deg = rad * 180/pi;
end

