 function [x,y]  =  fnMinMaxRow(x_grid,y_grid)
    % get the min and max x_grid value for each unique value in y_grid and
    % do lawnmower sort
    %[y_waypoint,idx] = sort(y_grid,'descend');
    %x_waypoint = x_grid(idx);
    uniq_y = unique(y_grid);
    %idx_y = flipud(idx_y);
   
    x=zeros(length(uniq_y)*2,1);
    y=zeros(length(uniq_y)*2,1);
    for i = 1: 1: length(uniq_y);
        
        row=x_grid(y_grid==uniq_y(i));
        if mod(i,2)
            x(i*2-1)=min(row);
            x(i*2)=max(row);
        else
            x(i*2-1)=max(row);
            x(i*2)=min(row);
        end
        
        y(i*2-1)=uniq_y(i);
        y(i*2)=uniq_y(i);
    end
   