function [boundaryArea_sorted] = fnSortboundaryArea(boundaryArea)

%method 1
%Step 1: Find the centroid:
cx = mean(boundaryArea(:,1));
cy = mean(boundaryArea(:,2));
%Step 2: Find the angles:
a = atan2(boundaryArea(:,2) - cy,  boundaryArea(:,1) - cx);
%Step 3: Find the correct sorted order:
[~, order] = sort(a);
%Step 4: Reorder the coordinates:
boundaryArea_sorted = [boundaryArea(order,1), boundaryArea(order, 2)];




%method 2
% 1.find the center of gravity C of the points
% 2.find direction from C to each point
% 3.get the direction as an angle from 0 to 2*pi
% 4.sort
%{
C = sum(boundaryArea) / size(boundaryArea, 1);
dv = bsxfun(@minus, boundaryArea, C);
a = atan2(dv(:,2), dv(:,1)) + pi;
[s si] = sort(a);
boundaryArea_sorted = boundaryArea(si, :);
%}
end

