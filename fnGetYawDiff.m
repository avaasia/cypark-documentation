function [sourceFile,yawDiff]=fnGetYawDiff (initialYaw,folderDir)

% Check to make sure that folder actually exists.  Warn user if it doesn't.
if ~isdir(folderDir)
  errorMessage = sprintf('Error: The following folder does not exist:\n%s', folderDir);
  uiwait(warndlg(errorMessage));
  return;
end

[status, cmdout] = system(['exiftool -s -csv -FlightYawDegree ' folderDir ' > long1.csv']);
if status ==0
    formatSpec = '%C%f';
    T = readtable('long1.csv','Delimiter',',','Format',formatSpec);
    sourceFile=char(T.SourceFile);
    yawDiff=abs(T.FlightYawDegree-initialYaw);
else
    error(cmdout);
end