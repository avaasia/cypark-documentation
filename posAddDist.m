function [deg_lat2,deg_lon2] = posAddDist(deg_lat1,deg_lon1,dist,deg_bearing)
%Formula:	lat2 = asin( sin lat1 ? cos ? + cos lat1 ? sin ? ? cos ? )
%lon2 = ?1 + atan2( sin ? ? sin ? ? cos lat1, cos ? ? sin lat1 ? sin lat2 )
%where	? is latitude, ? is longitude, ? is the bearing (clockwise from north), 
%? is the angular distance d/R; d being the distance travelled, R the earth�s radius


%http://www.movable-type.co.uk/scripts/latlong.html

R = 6374000; %earth radius in metres
a=dist/R;
rad_lat1 = deg_lat1*pi/180;
rad_lon1 = deg_lon1*pi/180;
rad_bearing=deg_bearing*pi/180;

rad_lat2 = asin( sin(rad_lat1) * cos(a) + cos(rad_lat1)*sin(a)*cos(rad_bearing) );
rad_lon2 = rad_lon1 + atan2( sin (rad_bearing)* sin (a)* cos (rad_lat1), cos (a) - sin (rad_lat1) * sin (rad_lat2) );

deg_lat2=rad_lat2*180/pi;
deg_lon2=rad_lon2*180/pi;