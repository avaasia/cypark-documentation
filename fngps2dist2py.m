function [ dist2py ] = fngps2dist2py( refPts )
%Compute the distance in m per px of a given points in a given blueprint
%   Detailed explanation goes here

TR = refPts.TR;
TL = refPts.TL;
BR = refPts.BR;
BL = refPts.BL;

% Pixel X direction (column-wise)
pixel_diff_XL = TL.XP - BL.XP;
pixel_diff_XR = TR.XP - BR.XP;

% Pixel Y direction (row-wise)
pixel_diff_YL = TL.YP - BL.YP;
pixel_diff_YR = TR.YP - BR.YP;

% Angle difference
angle_L = atan(pixel_diff_XL/pixel_diff_YL);
angle_R = atan(pixel_diff_XR/pixel_diff_YR);

% Total Distance Calculation
dist_diff_L = pos2dist(TL.LAT,TL.LON,BL.LAT,BL.LON,2)*1000;
dist_diff_R = pos2dist(TR.LAT,TR.LON,BR.LAT,BR.LON,2)*1000;

% Total Distance in X direction
dist_diff_XL = dist_diff_L * sin(angle_L);
dist_diff_XR = dist_diff_R * sin(angle_R);

% Total Distance in Y direction
dist_diff_YL = dist_diff_L * cos(angle_L);
dist_diff_YR = dist_diff_R * cos(angle_R);

% Distance per px
distppx_L = abs(dist_diff_XL/pixel_diff_XL);
distppx_R = abs(dist_diff_XR/pixel_diff_XR);

dist2py = mean([distppx_L distppx_R]);

end