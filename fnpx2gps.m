
function [ waypointsGPS ] = fnpx2gps( waypoints, r )
% This function is used when there is a blueprint with route planned.
% The function will convert the interpolated pixel value to interpolated
% GPS

%% Old Code
%r  = load('reference_frame5.mat'); %testing code, must change
%google = load('testGPS.mat'); %not in use

%% Save the reference points into corresponding corners
BL = r.frame.BL;
BR = r.frame.BR;
TR = r.frame.TR;
TL = r.frame.TL;

%not used anymore
%SP = r.frame.SP;

% saves the corresponding pixel and gps points in matrix for solving
PixelX = [BL.XP BR.XP TR.XP TL.XP];
PixelY = [BL.YP BR.YP TR.YP TL.YP];
Lat = [BL.LAT BR.LAT TR.LAT TL.LAT];
Lon = [BL.LON BR.LON TR.LON TL.LON];
%waypoints = [379,1538 ; 402,1537];

%% Old code
%{
x=1;
index = size(waypoints);
while x <= index(1)
    %reference from bottom left corner
    latBL(x) = google.lag{1} + (waypoints(x,1)-368)*SP.XLAT + (waypoints(x,2)-1546)*SP.YLAT;
    lonBL(x) = google.lon{1} + (waypoints(x,1)-368)*SP.XLON + (waypoints(x,2)-1546)*SP.YLON;
    x = x+1;
end

x=1;
index = size(waypoints);
while x <= index(1)
    %reference from bottom right corner
    latBR(x) = google.lag{4} + (waypoints(x,1)-733)*SP.XLAT + (waypoints(x,2)-1543)*SP.YLAT;
    lonBR(x) = google.lon{4} + (waypoints(x,1)-733)*SP.XLON + (waypoints(x,2)-1543)*SP.YLON;
    x = x+1;
end

x=1;
index = size(waypoints);
while x <= index(1)
    %reference from top right corner
    latTR(x) = google.lag{3} + (waypoints(x,1)-835)*SP.XLAT + (waypoints(x,2)-47)*SP.YLAT;
    lonTR(x) = google.lon{3} + (waypoints(x,1)-835)*SP.XLON + (waypoints(x,2)-47)*SP.YLON;
    x = x+1;
end

x=1;
index = size(waypoints);
while x <= index(1)
    %reference from top left corner
    latTL(x) = google.lag{2} + (waypoints(x,1)-469)*SP.XLAT + (waypoints(x,2)-45)*SP.YLAT;
    lonTL(x) = google.lon{2} + (waypoints(x,1)-469)*SP.XLON + (waypoints(x,2)-45)*SP.YLON;
    x = x+1;
end

x=1;
index = size(waypoints);
while x <= index(1)
   lat(x) = ((latBL(x)+ latBR(x)+ latTR(x)+latTL(x))/4);
   lon(x) = ((lonBL(x)+ lonBR(x)+ lonTR(x)+ lonTL(x))/4);
   x = x+1;
end
waypointsGPS = horzcat(lat',lon');

end
%}
% from cypark block 1 (testinig purposes)
% SP.XLAT = -5.038550789950699e-09;
% SP.XLON = 6.855191256942082e-07;
% SP.YLAT = -6.622238917370427e-07;
% SP.YLON = -3.839966760673033e-08;

%% Interpolate the GPS with corresponding reference points
% find out pixel location for point of interest
F_Lat = scatteredInterpolant(PixelX',PixelY',Lat')
F_Lon = scatteredInterpolant(PixelX',PixelY',Lon');

for i=1:length(waypoints)
    Lat_interp(i) = F_Lat(waypoints(i,1), waypoints(i,2));
    Lon_interp(i) = F_Lon(waypoints(i,1), waypoints(i,2));
end

%% Old Code
%{
x=1;
index = size(waypoints);
while x <= index(1)
    %reference from bottom left corner
    latBL(x) = BL.LAT + (waypoints(x,1)-BL.XP)*SP.XLAT + (waypoints(x,2)-BL.YP)*SP.YLAT;
    lonBL(x) = BL.LON + (waypoints(x,1)-BL.XP)*SP.XLON + (waypoints(x,2)-BL.YP)*SP.YLON;
    x = x+1;
end

x=1;
index = size(waypoints);
while x <= index(1)
    %reference from bottom right corner
    latBR(x) = BR.LAT + (waypoints(x,1)-BR.XP)*SP.XLAT + (waypoints(x,2)-BR.YP)*SP.YLAT;
    lonBR(x) = BR.LON + (waypoints(x,1)-BR.XP)*SP.XLON + (waypoints(x,2)-BR.YP)*SP.YLON;
    x = x+1;
end

x=1;
index = size(waypoints);
while x <= index(1)
    %reference from top right corner
    latTR(x) = TR.LAT + (waypoints(x,1)-TR.XP)*SP.XLAT + (waypoints(x,2)-TR.YP)*SP.YLAT;
    lonTR(x) = TR.LON + (waypoints(x,1)-TR.XP)*SP.XLON + (waypoints(x,2)-TR.YP)*SP.YLON;
    x = x+1;
end

x=1;
index = size(waypoints);
while x <= index(1)
    %reference from top left corner
    latTL(x) = TL.LAT + (waypoints(x,1)-TL.XP)*SP.XLAT + (waypoints(x,2)-TL.YP)*SP.YLAT;
    lonTL(x) = TL.LON + (waypoints(x,1)-TL.XP)*SP.XLON + (waypoints(x,2)-TL.YP)*SP.YLON;
    x = x+1;
end

x=1;
index = size(waypoints);
while x <= index(1)
   lat(x) = ((latBL(x)+ latBR(x)+ latTR(x)+latTL(x))/4);
   lon(x) = ((lonBL(x)+ lonBR(x)+ lonTR(x)+ lonTL(x))/4);
   x = x+1;
end
%}

% Saves GPS into a matrix
waypointsGPS = horzcat(Lat_interp',Lon_interp');

end
%}
