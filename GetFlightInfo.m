function flightInfo = GetFlightInfo( flightLog )
% Extract longitude, latitude, and altitude information from flight log
%   Input is just the fligt name (e.g. 'FLY214' not 'FLY214.csv')
%   Variables saved as 'flight.mat'
%   [flight_lon, flight_lat, flight_altabs, flight_altrel, fIdx]

flight_log = xlsread([flightLog '.csv']);
flight_lon = flight_log(:,4);
flight_lat = flight_log(:,5);
flight_altabs = flight_log(:,9);
flight_altrel = flight_log(:,11);

fIdx = isnan(flight_lon);
flight_lon(fIdx) = [];
flight_lat(fIdx) = [];
flight_altabs(fIdx) = [];
flight_altrel(fIdx) = [];

flightInfo.LON = flight_lon;
flightInfo.LAT = flight_lat;
flightInfo.ALTABS = flight_altabs;
flightInfo.ALTREL = flight_altrel;
end

