function [ r ] = fnReferenceFrameGPSSaving( r,lat,lon )
%Saves the reference GPS
            r.BL.LAT = lat(1);
            r.BL.LON = lon(1);
            r.TL.LAT = lat(2);
            r.TL.LON = lon(2);
            r.TR.LAT = lat(3);
            r.TR.LON = lon(3);
            r.BR.LAT = lat(4);
            r.BR.LON = lon(4);
end

