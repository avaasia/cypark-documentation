function fnGPS2XML(GPS,altitude)
%GPS is in n rows of (lat, lon)
%altitude is an integer 

altcol =ones( length(GPS),1) * altitude;
coordinates=[GPS altcol];

dlmwrite('waypoints.csv',coordinates,'precision','%.6f','delimiter',',');
system('cscript csv2route_mod.js waypoints.csv template.xml');