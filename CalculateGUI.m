function CalculateGUI()

fc = figure('Visible', 'off', 'HandleVisibility', 'on', 'Tag', 'calculator', ...
    'Position', [360, 500, 500, 350], 'Units', 'normalized');

ref_info = struct('IMG', 'str', 'LOC', 'str', 'XP', 0, 'YP', 0, 'LON', 0, 'LAT', 0, ...
'ALT', 0, 'GPSLAT', 0, 'GPSLON',0);
guidata(fc, ref_info);
lag = {0, 1.890316, 1.889111, 1.888829};
lon = {103.1559, 103.1559, 103.1573, 103.1573};

fwtext1 = uicontrol('Style', 'text', 'Position', [50, 230, 400, 100],...
    'String','Remember to enter the direction of displacement.', ...
    'FontSize', 12, 'ForegroundColor', 'r');

fwtext2 = uicontrol('Style', 'text', 'Position', [50, 208, 400, 100], ...
    'String', 'Left (for Horizontal) and Below (for Vertical) are negative.', ...
    'FontSize', 10, 'ForegroundColor', 'r');

 fcmenu = uicontrol('Style', 'popupmenu', 'String', {'Choose Reference Point', ...
     'Top-Left', 'Top-Right', 'Bottom-Left', 'Bottom-Right'},...
     'Position', [50, 170, 150, 90], 'FontSize', 10);
 
fclattext = uicontrol('Style', 'text', 'String', 'Latitude', ...
    'Position', [225, 255, 75, 25], 'FontSize', 10, 'HorizontalAlignment', 'left');

fclat = uicontrol('Style', 'edit', 'String', ' ', ...
    'Position', [215, 235, 75, 25], 'FontSize', 10, 'HorizontalAlignment', 'left');

fclontext = uicontrol('Style', 'text', 'String', 'Longitude', ...
    'Position', [305, 255, 75, 25], 'FontSize', 10, 'HorizontalAlignment', 'left');

fclon = uicontrol('Style', 'edit', 'String', ' ', ...
    'Position', [295, 235, 75, 25], 'FontSize', 10, 'HorizontalAlignment', 'left');

fccoord = uicontrol('Style', 'pushbutton', 'String', 'Store', ...
    'Position', [400, 235, 50, 25], 'FontSize', 10, ...
    'Callback', {@store_Callback});

fchooseb = uicontrol('Style', 'pushbutton', 'String', 'Choose Reference Image', ...
    'Position', [30, 195, 175, 25], 'FontSize', 10, 'Callback',{@showRefImages_Callback});

fcimbox = uicontrol('Style', 'edit', 'String', 'Img Name', ...
    'Position', [215, 195, 75, 25], 'Fontsize', 10, 'BackgroundColor', 'w');

fcxdtext = uicontrol('Style', 'text', 'String', 'Enter Horizontal Displacement', ...
    'Position', [5, 155, 200, 25], 'FontSize', 10, 'HorizontalAlignment', 'right');

fcxdis = uicontrol('Style', 'edit', 'String', ' ', ...
    'Position', [215, 160, 75, 25], 'FontSize', 10, 'HorizontalAlignment', 'left');

fcydtext = uicontrol('Style', 'text', 'String', 'Enter Vertical Displacement', ...
    'Position', [5, 115, 200, 25], 'FontSize', 10, 'HorizontalAlignment', 'right');

fcydis = uicontrol('Style', 'edit', 'String', ' ', ...
    'Position', [215, 120, 75, 25], 'FontSize', 10, 'HorizontalAlignment', 'left');

fcxptext = uicontrol('Style', 'text', 'String', 'Enter Horizontal Dimensions', ...
    'Position', [5, 75, 200, 25], 'FontSize', 10, 'HorizontalAlignment', 'right');

fcxdim = uicontrol('Style', 'edit', 'String', ' ', ...
    'Position', [215, 80, 75, 25], 'FontSize', 10, 'HorizontalAlignment', 'left');

fcyptext = uicontrol('Style', 'text', 'String', 'Enter Vertical Dimensions', ...
    'Position', [5, 35, 200, 25], 'FontSize', 10, 'HorizontalAlignment', 'right');

fcydim = uicontrol('Style', 'edit', 'String', ' ', ...
    'Position', [215, 40, 75, 25], 'FontSize', 10, 'HorizontalAlignment', 'left');
 
fccalc = uicontrol('Style', 'pushbutton', 'String', 'Calculate', ...
    'Position', [320, 150, 150, 25], 'FontSize', 10, ...
    'Callback', {@calculate_Callback});

fctex1 = uicontrol('Style','text', 'String', 'Enter these reference coordinates in Main GUI', ...
    'Position', [320, 110, 150, 30], 'FontSize', 10);

fccoor = uicontrol('Style', 'edit', 'String', ' ', ...
    'Position', [320, 70, 150, 25], 'FontSize', 10);

movegui(fc,[800 200])
fc.Name = 'Calculate Coordinates of Reference';
fc.Visible = 'on';

%coordinates from google earth, starting from bottom left, clockwise direction


    function store_Callback(hObject, event)
    data = guidata(hObject);
    str = fcmenu.String;
    val = fcmenu.Value;
    num = 0;
    switch str{val};
        case 'Bottom-Left'
                num = 1;
        case 'Top-Left'
                num = 2;
        case 'Top-Right'
                num = 3;
        case 'Bottom-Right'
                num = 4;
    end
    lagno = str2double(get(fclat, 'String'));
    lonno = str2double(get(fclon, 'String'));
    lag{num} = lagno;
    lon{num} = lonno;
    
    data.GPSLAT = lag;
    data.GPSLON = lon;
    
    guidata(hObject, data);
    
    save('testGPS', 'lag', 'lon');

end 
    
    function showRefImages_Callback(hObject, event)
        filename = uigetfile('RefImages\*.jpg');
        set(fcimbox, 'String', filename);
        data = guidata(hObject);
        
        % Extract GPS Information
        img_meta = imfinfo(['RefImages\' filename]);
        img_gps = img_meta.GPSInfo;
        LAT = img_gps.GPSLatitude(1) + (img_gps.GPSLatitude(2)/60) + (img_gps.GPSLatitude(3)/3600);
        LON = img_gps.GPSLongitude(1) + (img_gps.GPSLongitude(2)/60) + (img_gps.GPSLongitude(3)/3600);
        ALT = img_gps.GPSAltitude;
        
        GPS = load('testGPS.mat');
        % Identify the location
        for x = 1:4
            dist(x) = pos2dist(LAT,LON,GPS.lag{x},GPS.lon{x},1);
        end
       
        [M,I] = min(dist);
        switch I
            case 1
                Position = 'Bottom-Left';
            case 2
                Position = 'Top-Left';
            case 3
                Position = 'Top-Right';
            case 4
                Position = 'Bottom-Right';
        end
        
        data.IMG = filename;
        data.LOC = Position;
        data.LON = LON;
        data.LAT = LAT;
        data.ALT = ALT;
        
        guidata(hObject, data);
        
        figure;
        imshow(['RefImages\' filename],'Border', 'tight');  
        center = [1500, 2000] ;
        ustringedge = uicontrol('Style','text',...
          'Position',[100 100 110 300],...
          'String','Click the edge','FontSize', 10);
      
        zoom (2);
        edge = ginput(1) ;
        
        Horizontal_displacement = center(2) - edge(1);
        Vertical_displacement = edge(2)- center(1) ;
        
        displacement_text = {'Horizontal Displacement', ...
            Horizontal_displacement,'Vertical Displacement', Vertical_displacement};
        set(ustringedge, 'String', displacement_text);
        set(fcxdis, 'String', Horizontal_displacement);
        set(fcydis, 'String', Vertical_displacement);
        
        ustringHdim = uicontrol('Style','text',...
          'Position',[100 200 110 70],...
          'String','Horizontal Dimension of solar panel','FontSize', 10);
      
        Hori_dim=ginput(2) ;
        Hdim_text = {'Horizontal Dimension of solar panel', pdist(Hori_dim)};
        set(ustringHdim, 'String', Hdim_text);
        set(fcxdim, 'String', pdist(Hori_dim));
        
        ustringVdim= uicontrol('Style','text',...
          'Position',[100 100 110 70],...
          'String','Vertical Dimension of solar panel','FontSize', 10);
      
        Vert_dim=ginput(2) ;
        Vdim_text = {'Vertical Dimension of solar panel', pdist(Vert_dim)};
        set(ustringVdim, 'String', Vdim_text);
        set(fcydim, 'String', pdist(Vert_dim));
        
    end

    function calculate_Callback(hObject, eventdata)
        %data = guidata(hObject);
        cobj = findobj('Tag', 'operator');
        if ~isempty(cobj)
            cdata = guidata(cobj);
            data = guidata(hObject);
            Img = cdata.I;
            I = imread(Img);
        end
         blueprint = fnAsBuiltExtraction( I );
        regProp = fnRegionProps( blueprint.bluePrint,'Area', 'Centroid', 'boundingBox', 'Extrema');
         %find which rows has the smallest x coordinate
        rowx= find(regProp.Centroids==min(regProp.Centroids(:,1)));
        
        %Find the corresponding y coordinate
        MinVal_Y=regProp.Centroids(rowx(1),2); 
        for x1 = 1:size(rowx,1)
            if regProp.Centroids(rowx(x1),2)<MinVal_Y; 
            MinVal_Y=regProp.Centroids(rowx(x1),2); 
            end
        end
        Left_TopCent = [min(regProp.Centroids(:,1)), MinVal_Y];
        indexLT = find(ismember(regProp.Centroids,Left_TopCent,'rows'));
        Left_TopCoord = [regProp.BoundingBox(indexLT),regProp.BoundingBox(indexLT,2)];
        
        %finding the top right corner coordinates
        rowTop= find(regProp.BoundingBox(indexLT,2)== regProp.BoundingBox(:,2));
        MaxVal_X = regProp.BoundingBox(indexLT);
         for x2 = 1:size(rowTop,1)
            if regProp.BoundingBox(rowTop(x2),1)> MaxVal_X; 
            MaxVal_X=regProp.BoundingBox(rowTop(x2),1); 
            end
        end
        Right_TopCorner = [MaxVal_X + regProp.BoundingBox(rowTop(x2),3), regProp.BoundingBox(indexLT,2)];
        
        %finding the bottom left corner coordinates
        rowy= find(regProp.Centroids(:,2)==max(regProp.Centroids(:,2)));
        MinVal_X=regProp.Centroids(1,1); 
        for x3 = 1:size(rowy,1)
            if regProp.Centroids(rowy(x3),1)<MinVal_X; 
            MinVal_X=regProp.Centroids(rowy(x3),1); 
            end
        end
        Bottom_LeftCent = [MinVal_X, max(regProp.Centroids(:,2))];
        indexBL = find(ismember(regProp.Centroids,Bottom_LeftCent,'rows'));
        Bottom_LeftCoord = [regProp.BoundingBox(indexBL), ...
            (regProp.BoundingBox(indexBL,2)+regProp.BoundingBox(indexBL,4))];
        
        %finding the bottom right corner coordinates
        rowBtm= find(regProp.BoundingBox(indexBL,2)== regProp.BoundingBox(:,2));
        MaxVal_X2 = regProp.BoundingBox(indexBL);
         for x4 = 1:size(rowBtm,1)
            if regProp.BoundingBox(rowBtm(x4),1)> MaxVal_X2; 
            MaxVal_X2=regProp.BoundingBox(rowBtm(x4),1); 
            end
        end
        Right_BtmCorner = [MaxVal_X2 + regProp.BoundingBox(rowBtm(x4),3), ... 
            (regProp.BoundingBox(indexBL,2)+regProp.BoundingBox(indexBL,4))];
        
        startRow = [Bottom_LeftCoord ; Bottom_LeftCoord];
           
        data.startRow = startRow;
       
        switch data.LOC
            case 'Top-Left'
                xcorner = 469; %Left_TopCoord(1); %141
                ycorner = 45; %Left_TopCoord(2); %234
            case 'Top-Right'
                xcorner = 835;%Right_TopCorner(1); %2269
                ycorner = 47; %Right_TopCorner(2); %234
            case 'Bottom-Left'
                xcorner = 368; %Bottom_LeftCoord(1); %141
                ycorner = 1546; %Bottom_LeftCoord(2); %778
            case 'Bottom-Right'
                xcorner = 733; %Right_BtmCorner(1); %2269
                ycorner = 1543; %Right_BtmCorner(2); %778
        end
      
        xdim = str2double(get(fcxdim, 'String'));
        ydim = str2double(get(fcydim, 'String'));
        xdis = str2double(get(fcxdis, 'String'));
        ydis = str2double(get(fcydis, 'String'));
        
        xcoor = xcorner + (xdis/xdim)*mean(regProp.BoundingBox(:,3))
        ycoor = ycorner - (ydis/ydim)*mean(regProp.BoundingBox(:,4)) %reverse polarity for vertical due to matrix to graph conversion
        
        xcoor = round(xcoor);
        ycoor = round(ycoor);
        
        set(fccoor, 'String', [ num2str(xcoor) ',' num2str(ycoor)]);
        data.XP = xcoor;
        data.YP = ycoor;
        
        guidata(hObject, data);
    end

end

