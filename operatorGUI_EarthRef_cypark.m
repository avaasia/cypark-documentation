
function operatorGUI

h = figure('Visible', 'off', 'HandleVisibility', 'on', 'Tag', 'operator', ...
    'Position', [350, 500, 1200, 600], 'Units', 'normalized', 'Name', 'OperatorGUI');

ref_info = struct('IMG', 'str', 'XP', 0, 'YP', 0', 'LON', 0, 'LAT', 0, 'ALT', 0);
span = struct('XLON', 0, 'XLAT', 0, 'YLON', 0, 'YLAT', 0);

reference_frame.TL = ref_info;
reference_frame.TR = ref_info;
reference_frame.BL = ref_info;
reference_frame.BR = ref_info;
reference_frame.SP = span;

userData = struct('currentPt', ref_info,...
    'currentPOS', 'str', ...
    'currentMarker', struct, ...
    'reference_frame', reference_frame, ...
    'flightname', 'str');

guidata(h, userData);

S = load('Blueprint Sorted Information.mat');
hax = axes('Units', 'Pixels', 'Position', [100, 20, 1000,450], 'Units', 'normalized');
hBlueprint = imshow(S.BW, 'Parent', hax);
hold on;

hStatus = uicontrol('Style', 'text', 'String', {' ','Status Bar',' ',...
    'Store all the reference images in a subfolder titled "RefImages"' ...
    'and all the images for analysis in a subfolder titled "Images"'}, ...
    'Position', [101, 446, 198, 128], 'Units', 'normalized', ...
    'BackgroundColor', 'w', 'FontSize', 10, 'ForegroundColor', 'r');

hShowImg = uicontrol('Style', 'pushbutton', 'String', 'Show Calculator GUI', ...
    'Position', [330, 550, 150, 25], 'Units', 'normalized', 'FontSize', 10, ...
    'Callback', {@showRefImages_Callback});

hImport = uicontrol('Style', 'pushbutton',  'String', 'Import Data from Calc', ...
    'Position', [490, 550, 150, 25], 'Units', 'normalized', 'FontSize', 10, ...
    'Callback', {@importInfo_Callback});

%{
hImg = uicontrol('Style', 'text', 'String', 'Image Name', ...
        'Position', [330, 515, 150, 25], 'Units', 'normalized', ...
        'BackgroundColor', 'w', 'FontSize', 10);
    
hrefCoor = uicontrol('Style', 'edit', 'String', 'Ref Coordinates', ...
    'Position', [490, 515, 150, 25], 'Units', 'normalized', 'FontSize', 10);
%}
    
%old position [490, 480, 150, 25]
hplotPt = uicontrol('Style', 'pushbutton', 'String', 'Plot Reference Point', ...
    'Position', [490, 515, 150, 25], 'Units', 'normalized', 'FontSize', 10, ...
    'Callback', {@refPos_Callback});

%old position [330, 480, 150, 25]
hclearRef = uicontrol('Style', 'pushbutton', 'String', 'Clear Current Point', ...
    'Position', [330, 515, 150, 25], 'Units', 'normalized', 'FontSize', 10,...
    'Callback', {@clearRef_Callback});

%{
hstoreRef = uicontrol('Style', 'pushbutton', 'String', 'Store Reference Point', ...
    'Position', [330, 445, 150, 25], 'Units', 'normalized', 'FontSize', 10, ...
    'Callback', {@storeRef_Callback});

hSaveRef = uicontrol('Style', 'pushbutton', 'String', 'Save Reference Frame', ...
    'Position', [490, 445, 150, 25], 'Units', 'normalized', 'FontSize', 10, ...
    'Callback', {@saveRefFrame_Callback});
%}
hSelect = uicontrol('Style', 'pushbutton', 'String', 'Import Blueprint', ...
    'Position', [670, 580, 150, 25], 'Units', 'normalized', 'FontSize', 10, ...
    'Callback', {@importBlueprint_Callback});

hRefresh = uicontrol('Style', 'pushbutton', 'String', 'Refresh Blueprint', ...
    'Position', [670, 550, 150, 25], 'Units', 'normalized', 'FontSize', 10, ...
    'Callback', {@refreshBlueprint_Callback});

hLoadRefFrame = uicontrol('Style', 'pushbutton', 'String', 'Load Reference Frame', ...
    'Position', [830, 550, 150, 25], 'Units', 'normalized', 'FontSize', 10, ...
    'Callback', {@loadRefFrame_Callback});

% old position [670, 515, 150, 25]
hShowFly = uicontrol('Style', 'pushbutton', 'String', 'Choose Flight Log', ...
    'Position', [670, 480, 150, 25], 'Units', 'normalized', 'FontSize', 10, ...
    'Callback', {@showFly_Callback});

hFlyName = uicontrol('Style', 'text', 'String', 'Flight Log Name', ...
    'Position', [830, 515, 150, 25], 'Units', 'normalized', 'FontSize', 10, 'BackgroundColor', 'w');

% old position [670, 480, 150, 25]
hExtractFly = uicontrol('Style', 'pushbutton', 'String', 'Extract Flight Log', ...
    'Position', [830, 480, 150, 25], 'Units', 'normalized', 'FontSize', 10, ...
    'Callback', {@extractFly_Callback});

%old position [830, 480, 150, 25]
hPlotFly = uicontrol('Style', 'pushbutton', 'String', 'Plot Flight Log', ...
    'Position', [990, 480, 150, 25], 'Units', 'normalized', 'FontSize', 10, ...
    'Callback', {@plotFly_Callback});

hExtractImg = uicontrol('Style', 'pushbutton', 'String', 'Extract Images', ...
    'Position', [990, 550, 150, 25], 'Units', 'normalized', 'FontSize', 10, ...
    'Callback', {@extractImg_Callback});

%{
hPlotImg = uicontrol('Style', 'pushbutton', 'String', 'Plot Images', ...
    'Position', [990, 515, 150, 25], 'Units', 'normalized', 'FontSize', 10, ...
    'Callback', {@plotImg_Callback});
%}
    
% old position [990, 480, 150, 25]
hAlarms = uicontrol('Style', 'pushbutton', 'String', 'Show Alarms', ...
    'Position', [990, 515, 150, 25], 'Units', 'normalized', 'FontSize', 10, ...
    'Callback', {@showAlarms_Callback});

% old position [670, 515, 150, 25]
hGenerateCSV = uicontrol('Style', 'pushbutton', 'String', 'Generate Waypoints', ...
    'Position', [670, 515, 150, 25], 'Units', 'normalized', 'FontSize', 10, ...
    'Callback', {@GenerateWaypoints_Callback});

hPlotmodifiedImg = uicontrol('Style', 'pushbutton', 'String', 'Shift Image Plot', ...
    'Position', [990, 445, 150, 25], 'Units', 'normalized', 'FontSize', 10, ...
    'Callback', {@plotmodifiedImg_Callback});
    
    
    function showRefImages_Callback(hObject, eventdata, handles)       
        InputGUI;       % opens second GUI to key in the pixels and GPS coordinates
%         outString = textwrap(hStatus, {' ', 'Please key in the mapped pixel coordinates and choose the corresponding position.'});
%         set(hStatus, 'String', outString);
    end

    function importInfo_Callback(hObject, eventdata, handles)
        cobj = findobj('Tag', 'calculator');
        if ~isempty(cobj)
            cdata = guidata(cobj);
            data = guidata(hObject);
            data.currentPOS = cdata.LOC;
            data.currentPt.IMG = cdata.IMG;
            data.currentPt.XP = cdata.XP;
            data.currentPt.YP = cdata.YP;
            data.currentPt.LON = cdata.LON;
            data.currentPt.LAT = cdata.LAT;
            data.currentPt.ALT = cdata.ALT;
            data.startRow = cdata.startRow;
            set(hImg, 'String', data.currentPt.IMG);
            set(hrefCoor, 'String', [num2str(data.currentPt.XP) ',' num2str(data.currentPt.YP)]);
            guidata(hObject, data);
            outString = textwrap(hStatus, {' ', 'Object Found', 'Imported Data'});
            set(hStatus, 'String', outString);
        else 
            outString = textwrap(hStatus, {' ', 'Object not Found. Did you close the Calculator GUI?'});
            set(hStatus, 'String', outString);
        end
    end

    function refPos_Callback(hObject, eventdata, handles)
        % plot the coordinates and store the information
        data = guidata(hObject);
        xc = data.currentPt.XP;
        yc = data.currentPt.YP;
        hr = plot(xc, yc, '*', 'MarkerSize', 10);
        data.currentMarker = hr;
        guidata(hObject, data);
        outString = textwrap(hStatus, {' ',['Plotted ' data.currentPOS ' Point.'], ...
                ' ', 'Remember to store the point before choosing the next image.'});
        set(hStatus, 'String', outString);    
    end

    function clearRef_Callback(hObject, eventdata, handles)
        % if not happy with the point, clear it and plot again
        data = guidata(hObject);
        delete(data.currentMarker);
        data.currentPt = struct('IMG', 'str', 'XP', 0, 'YP', 0', 'LON', 0, 'LAT', 0, 'ALT', 0);
        guidata(hObject, data);
    end
%{
    function storeRef_Callback(hObject, eventdata, handles)
        % if happy, store the current point, and move onto calculating the
        % next point.  Stores in UI memory, not main memory
        data = guidata(hObject);        
        cobj = findobj('Tag', 'calculator');
        if ~isempty(cobj)
            cdata = guidata(cobj);
            lat = cdata.GPSLAT;
            lon = cdata.GPSLON;
            
            % Setting the reference frame blueprint coordinates
            reference_frame = fnReferenceFrameGPSSaving(reference_frame,lat,lon);
            data.reference_frame = reference_frame;

            if strcmp(data.currentPt.IMG,'str')
                outString = textwrap(hStatus, {' ','Import Data from Calculator First'});
                set(hStatus, 'String', outString);
            end

            filename = data.imageName;
            save(filename, 'reference_frame');
        end
        switch data.currentPOS
            case 'Top-Left'
                data.reference_frame.TL = data.currentPt;
            case 'Top-Right'
                data.reference_frame.TR = data.currentPt;
            case 'Bottom-Left'
                data.reference_frame.BL = data.currentPt;
            case 'Bottom-Right'
                data.reference_frame.BR = data.currentPt;
        end
        guidata(hObject, data);
        outString = textwrap(hStatus, {' ','Stored point in UI memory.', ' ', ...
            'If you have completed all 4 points, remember to save reference frame to main memory.'});
        set(hStatus, 'String', outString);
    end

    function saveRefFrame_Callback(hObject, eventdata, handles)
        % if all four points are done save the points to main memory
        data = guidata(hObject);
        guidata(hObject, data);
        frame = data.reference_frame;
        save('reference_frame', 'frame');
        
        outString = textwrap(hStatus, {' ','Saved reference frame in main memory.', ' ', ...
            'You will be able to retrieve the frame for later use.'});
        set(hStatus, 'String', outString);
    end
%}
function importBlueprint_Callback(hObject, eventdata, handles)
     delete(hBlueprint);
        data = guidata(hObject);
        filename = uigetfile('BlueprintBuilding\*.png;*.jpg');
        imageName = strsplit(filename, '.jpg');
        if strcmp(imageName,filename);
            imageName = strsplit(filename, '.png');
        end
        data.imageName = imageName{1};
        I = ['BlueprintBuilding\' filename];
        data.I = I;
        hBlueprint = imshow(I);
        Img = data.I;
        I = imread(Img);
                
        % Process the blueprint to obtain the area contains panels only
        blueprint = fnAsBuiltExtraction( I );
        regProp = fnRegionProps( blueprint.bluePrint,'Area', 'Centroid', 'boundingBox', 'Extrema');
        data.BoundingBox = regProp.BoundingBox;
        boundaryCorner = corner(blueprint.boundingArea);
        
        %plotting the boundary points
        %plot(boundaryCorner(:,1), boundaryCorner(:,2), 'b*');
        
        % Find the reference coordinates in the blueprint
        ReferenceCoor = fnReferenceCoordinateFinding(regProp);

        % Setting the reference frame blueprint coordinates
        reference_frame = fnReferenceFrameCoorSaving(reference_frame,ReferenceCoor);

        % Plot the 4 corners of the reference frame
        fnReferenceFramePlotting(reference_frame);
        
        data.reference_frame = reference_frame;
        data.boundaryCorner = boundaryCorner;
        guidata(hObject, data);
        hold on;
    end

    function refreshBlueprint_Callback(hObject, eventdata, handles)
        hold off;
        delete(hBlueprint);
        data = guidata(hObject);
        I = data.I;
        hBlueprint = imshow(I);
        hold on;
    end

    function loadRefFrame_Callback(hObject, eventdata, handles)
        data = guidata(hObject);
        
        % load and plot pre-existing reference frame from main memory
        r  = load(['reference_frameblock' data.imageName '.mat']);
     
        % Plot the 4 corners of the reference frame
        fnReferenceFramePlotting(r.frame);
        
        % Storing of information to data GUI
        data.reference_frame = r.frame;
        guidata(hObject, data);
        
        outString = textwrap(hStatus, {' ','Loaded reference frame.', ' ', 'Load flight path for plotting.'});
        set(hStatus, 'String', outString);
    end

    function showFly_Callback(hObject, eventdata, handles)
        % Choose which flight to plot
        filename = uigetfile('FLY*.*');
        flightname = strsplit(filename, '.');
        flightname = strsplit(flightname{1}, '_');
        flightname = flightname{1};
        set(hFlyName, 'String', flightname);
        data = guidata(hObject);
        data.flightname = flightname;
        guidata(hObject, data);
        
        outString = textwrap(hStatus, {' ','Chosen Flight Log', 'Click on "Extract Flight Path" if this is a new flight.', ...
            'Or on "Plot Flightpath" is extraction was done earlier.'});
        set(hStatus, 'String', outString);
    end

    function extractFly_Callback(hObject, eventdata, handles)
        
        data = guidata(hObject);
        flightname = data.flightname;        
        r  = load(['reference_frameblock' data.imageName '.mat']);
        data.reference_frame = r.frame;
        
        % Extract the flight log info from the CSV file
        % Will take a while
        % Following info saved:
            % longitude
            % latitude
            % absolute altitude
            % relative altitude
        flightInfo = GetFlightInfo(flightname);
        save([flightname '_info'], 'flightInfo');
        
        outString = textwrap(hStatus, {' ', 'Extracted csv.', 'Converting to pixel coordinates.'});
        set(hStatus, 'String', outString);
        
        % Solve simultaneous equations to convert from gps to pixel
        % coordinates > will take a while
        flightpath = ConvertFlightInfo(flightInfo, data.reference_frame);
        save([flightname '_converted'], 'flightpath');
                
        outString = textwrap(hStatus, {' ', 'Converted to pixel coordinates.', 'Adjusting for altitude.'});
        set(hStatus, 'String', outString);
        
        % Adjust the points for altitude (using the regression)
        flightpath = AdjustFlightPath(flightpath);
        save([flightname '_FLIGHTPATH'], 'flightpath');
        
        outString = textwrap(hStatus, {' ', 'Completed adjusting.', 'You can now plot the flightpath.'});
        set(hStatus, 'String', outString);
    end

    function plotFly_Callback(hObject, eventdata, handles)
        data = guidata(hObject);
        f = load([data.flightname '_FLIGHTPATH']);
        flight = f.flightpath;
        plot(flight.X, flight.Y, 'g', 'LineWidth', 2);
        
        outString = textwrap(hStatus, {' ', 'Plotted Flightpath'});
        set(hStatus, 'String', outString);
    end

    function extractImg_Callback(hObject, eventdata, handles)
        data = guidata(hObject);
        r  = load(['reference_frameblock' data.imageName '.mat']);
        data.reference_frame = r.frame;
        
        % extract metadata of images of interest
        imageInfo = getImageInfo;
        save([data.flightname '_imageInfo'], 'imageInfo');
        outString = textwrap(hStatus, {' ', 'Completed extracting metadata of images.'});
        set(hStatus, 'String', outString);
        
        % calculate pixel from gps, adjust for altitude
        images = convertImages(1, imageInfo, data.reference_frame);
        
        % plot the images on the blueprint
        plot(images.X, images.Y, 'ko', 'MarkerSize', 10, 'LineWidth', 2);
        
        % message to the status
        outString = textwrap(hStatus, {' ', 'See the Images.'});
        
        % save the flightname images
        save([data.flightname '_IMAGES'], 'images');
        
        guidata(hObject, data);
        
        %outString = textwrap(hStatus, {' ', 'Completed converting coordinates of images.'});
        set(hStatus, 'String', outString);
    end
%{
    function plotImg_Callback(hObject, eventdata, handles) 
        data = guidata(hObject);
        im = load([data.flightname '_IMAGES']);
        imgs = im.images;
        plot(imgs.X, imgs.Y, 'ko', 'MarkerSize', 10, 'LineWidth', 2);
        outString = textwrap(hStatus, {' ', 'See the Images.'});
        set(hStatus, 'String', outString);
    end
%}
    function showAlarms_Callback(hObject, eventdata, handles) 
        data = guidata(hObject);
        flightname = data.flightname;
        
        im = load([flightname '_imageInfo']);
        r  = load(['reference_frameblock' data.imageName '.mat']);
        
        % find all the relevant alarms
        alarms = findAlarms(im.imageInfo, r.frame);
        save([flightname '_ALARMS'], 'alarms');
        
        im = load([data.flightname 'modified_IMAGES']);
        imgs = im.images;
        
        fnShowAlarms(alarms,imgs);
        
        set(hStatus, 'String', {' ', 'See the Alarms.'});
    end

    function plotmodifiedImg_Callback(hObject, eventdata) 
        data = guidata(hObject);
        im = load([data.flightname '_IMAGES']);
        images = im.images;
        promptHdisplace = 'Horizontal Displacement in terms of panels?';
        Hdisplace = input(promptHdisplace);
        promptVdisplace = 'Vertical Displacement in terms of panels?';
        Vdisplace = input(promptVdisplace);
        images.X = Hdisplace*(mean(data.BoundingBox(:,3))+2) + images.X;
        images.Y = Vdisplace*(mean(data.BoundingBox(:,4))+2) + images.Y + floor(Vdisplace/3)*5;
        oldplot = findobj('type','line');
        delete(oldplot);
        plot(images.X, images.Y, 'ko', 'MarkerSize', 10, 'LineWidth', 2);
        save([data.flightname 'modified_IMAGES'], 'images'); 
    end

    function GenerateWaypoints_Callback(hObject, eventdata, handles)
        data = guidata(hObject);
        
        % load reference frame from main memory
        r  = load(['reference_frameblock' data.imageName '.mat']);
        boundaryCorner = data.boundaryCorner;
        
        % Prompt for distance and altitude
        prompt1 = 'What is forward distance? ';
        fwdDist = input(prompt1);
        
        prompt2 = 'What is side distance? ';
        sideDist = input(prompt2);
        
        prompt3 = 'What is the altitude? ';
        altitude = input(prompt3);
      
        % Storing into the relative points
        refPts = r.frame;
        
        startRow = data.startRow;       %[108,899; 337,899]; %block1 [379, 1538; 721, 1538]; 
        
        % Sort and index the boundaryArea
        boundaryCorner = fnSortboundaryArea(boundaryCorner);
        index = size(boundaryCorner);
  
        for y = 1:2
            for x = 1: index(1)-2
                X = [boundaryCorner(x,:);boundaryCorner(x+1,:)];
                d1(x) = pdist(X,'euclidean');
                Y = [boundaryCorner(x,:);boundaryCorner(x+2,:)];
                d2(x) = pdist(Y,'euclidean');
                 if d1(x) > d2(x)
                    temp = boundaryCorner(x+1,:);
                    boundaryCorner(x+1,:) = boundaryCorner(x+2,:);
                    boundaryCorner(x+2,:) = temp ;
                end
            end
          end
 
        boundaryCorner((index(1) +1),:) = boundaryCorner(1,:);
        
        % Convert the GPS to dist/px
        dist2PxRatio = fngps2dist2px( refPts )
        
        % Waypoints generation
        waypoints = fnGeneratePxWaypoints (boundaryCorner, startRow, fwdDist, sideDist,1/dist2PxRatio);
        save('waypoints1', 'waypoints');
        
        % Convert waypoint generated in pixel to GPS
        waypointsGPS = fnpx2gps(waypoints,r);
        
        % Convert GPS to XML code for importing into UGCS
        fnGPS2XML(waypointsGPS,altitude)

    end
movegui(h,'center')
h.Visible = 'on';

end

