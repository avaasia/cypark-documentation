function [ info ] = fnExtractExifInfo(varargin)
%Extract the camera pitch degree from the exif information
%Exif tool extract information and store into string
folderDir = 'Images\*jpg'; %%by default folderDir is Images

%if input folder is given, change to batch extract that folder, the folder
%must be in the GUI folder
if nargin == 1
A = strsplit(varargin{1},'\');
lastfolder = size(A);
folderDir = strcat(A{lastfolder(2)}, '\*jpg');
end

[status, cmdout] = system(['exiftool -s -csv -GimbalPitchDegree -GimbalYawDegree -GimbalRollDegree -FlightPitchDegree -FlightYawDegree -FlightRollDegree -RelativeAltitude ' ...
    folderDir ' > long1.csv']);
%import csv and find yaw diff
if status ==0
formatSpec = '%C%f%f%f%f%f%f%f';
T = readtable('long1.csv','Format',formatSpec);
sourceFile=char(T.SourceFile);
info.CP= T.GimbalPitchDegree;
info.CY= T.GimbalYawDegree;
info.CR= T.GimbalRollDegree;
info.DP= T.FlightPitchDegree;
info.DY= T.FlightYawDegree;
info.DR= T.FlightRollDegree;
info.alt= T.RelativeAltitude;
else
error(cmdout);
end
%{
    [status, cmdout] = system(['exiftool -GimbalPitchDegree ' filename]);
    
    %Extract the info from the string
    [C, matches] = strsplit(cmdout,{'Gimbal Pitch Degree',' ',':',''});
    info = str2double(C(2));
end

%}

 