function [ ] = fnShowAlarms( alarms,imgs )
%Show the alarms if the images are too far off from the previous(tiime,
%distance, or angle is not right

%{
        % Images that are out of the block reference frame
        lonAL = alarms.LONALARM;
        plot(imgs.X(lonAL), imgs.Y(lonAL), 'yo', 'MarkerSize', 10, 'LineWidth', 2);
        latAL = alarms.LATALARM;
        plot(imgs.X(latAL), imgs.Y(latAL), 'yo', 'MarkerSize', 10, 'LineWidth', 2);
        
%}
        % Time compared to the previous
        timeAL = alarms.TIMEALARM;
        plot(imgs.X(timeAL), imgs.Y(timeAL), 'ro', 'MarkerSize', 10, 'LineWidth', 2);
        plot(imgs.X(timeAL-1), imgs.Y(timeAL-1), 'ro', 'MarkerSize', 10, 'LineWidth', 2);
        e = 'Missing Image';
        text(imgs.X(timeAL)+5e-6, imgs.Y(timeAL)-20, e);
        
        %Distance
        distAL = alarms.DISTALARM;
        plot(imgs.X(distAL), imgs.Y(distAL), 'bo', 'MarkerSize', 10, 'LineWidth', 2);
        e = 'Exceed 8m';
        text(imgs.X(distAL)+5e-6, imgs.Y(distAL)-20, e);
        
        % Pitch angle of the camera
        angleAL = alarms.ANGALARM;
        plot(imgs.X(angleAL), imgs.Y(angleAL), 'go', 'MarkerSize', 10, 'LineWidth', 2);
        e = 'Angle Error';
        text(imgs.X(angleAL)+5e-6, imgs.Y(angleAL)-20, e);
        

end

