function [ flightpath_adj] = AdjustFlightPath( flightpath )
mx = -0.2768;
Kx = 9.7723;

my = 1.5589;
Ky = -33.026;

flightpath_adj = struct;
flightpath_adj.X = flightpath.X - mx*flightpath.ALTABS - Kx;
flightpath_adj.Y = flightpath.Y - my*flightpath.ALTABS - Ky;
flightpath_adj.ALTABS = flightpath.ALTABS;
flightpath_adj.ALTREL = flightpath.ALTREL;

end

