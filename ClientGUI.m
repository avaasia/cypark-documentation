function varargout = ClientGUI(varargin)
% ClientGUI MATLAB code for ClientGUI.fig
%      ClientGUI, by itself, creates a new ClientGUI or raises the existing
%      singleton*.
%
%      H = ClientGUI returns the handle to a new ClientGUI or the handle to
%      the existing singleton*.
%
%      ClientGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ClientGUI.M with the given input arguments.
%
%      ClientGUI('Property','Value',...) creates a new ClientGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ClientGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ClientGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ClientGUI

% Last Modified by GUIDE v2.5 06-Jul-2017 11:20:41

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ClientGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @ClientGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ClientGUI is made visible.
function ClientGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ClientGUI (see VARARGIN)

% Choose default command line output for ClientGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ClientGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ClientGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Next.
function Next_Callback(hObject, eventdata, handles)
% hObject    handle to Next (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global srcfilesvisual
global images
handles = guidata(hObject);
counter = handles.counter + 1;
srcfilesthermal = handles.srcfilesthermal;

%check if the current image is the last image
a = size(srcfilesthermal)
if counter > a(2)
    counter = counter - 1;
    userPrompt = sprintf('Last image');
	reply = questdlg(userPrompt, 'Last image', 'Ok', 'Last image ');
end
handles.counter = counter;
guidata(hObject, handles);

%Type of defect
set(handles.text4, 'String', 'Defect Test Next');

%displays visual image
axes(handles.axes1)
imshow(srcfilesvisual{counter})

%displays thermal image and plots a boundingbox of it on the blueprint
axes(handles.axes2)
RBG = LoadImages(handles.counter, handles.srcfilesthermal,handles.Blueprint, handles.frame,handles.ImageInfo);
axes(handles.axes3)
imshow(RBG);

% oldplot = findobj('type','line');
% delete(oldplot);
% axes(handles.axes3)
% plot(images.X(counter), images.Y(counter), 'ko', 'MarkerSize', 10, 'LineWidth', 2);



% --- Executes on button press in Previous.
function Previous_Callback(hObject, eventdata, handles)
% hObject    handle to Previous (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global srcfilesvisual
global images
handles = guidata(hObject)
counter = handles.counter - 1;
srcfilesthermal = handles.srcfilesthermal;

%check if the current image is the first image
a = size(srcfilesthermal)
if counter < 1
    counter = counter + 1;
    userPrompt = sprintf('First image');
	reply = questdlg(userPrompt, 'First image', 'Ok', 'First Image');
end
handles.counter = counter
guidata(hObject, handles);

%Type of defect
set(handles.text4, 'String', 'Defect Test Previous');

%displays visual image
axes(handles.axes1)
imshow(srcfilesvisual{counter})

%displays thermal image and plots a boundingbox of it on the blueprint
axes(handles.axes2)
RBG = LoadImages(handles.counter, handles.srcfilesthermal,handles.Blueprint, handles.frame,handles.ImageInfo);
axes(handles.axes3)
imshow(RBG);

% oldplot = findobj('type','line');
% delete(oldplot);
% axes(handles.axes3)
% plot(images.X(counter), images.Y(counter), 'ko', 'MarkerSize', 10, 'LineWidth', 2);

% --- Executes on button press in Visual_image.
function Visual_image_Callback(hObject, eventdata, handles)
% hObject    handle to Visual_image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global srcfilesvisual
d = uigetdir(pwd, 'Select a folder');
files = dir(fullfile(d, '*.jpg'));
for i = 1 : length(files)
  filename = strcat(d,'\' ,files(i).name);
  srcfilesvisual{i} = filename;
end
axes(handles.axes1)
imshow(srcfilesvisual{1})


% --- Executes on button press in Thermal_image.
function Thermal_image_Callback(hObject, eventdata, handles)
% hObject    handle to Thermal_image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

d = uigetdir(pwd, 'Select a folder')
files = dir(fullfile(d, '*.jpg'));
for i = 1 : length(files)
  filename = strcat(d,'\' ,files(i).name);
  srcfilesthermal{i} = filename;
end
axes(handles.axes2)
% imshow(srcfilesthermal{1});
handles = guidata(hObject);
load(['reference_frame' handles.imageName '.mat'])
handles.frame = frame;
handles.counter = 1;
handles.srcfilesthermal = srcfilesthermal;
info = fnExtractExifInfo(d);
handles.ImageInfo = info;
RBG = LoadImages(handles.counter, handles.srcfilesthermal,handles.Blueprint, handles.frame,handles.ImageInfo);
axes(handles.axes3)
imshow(RBG);
%{
    for i=1:1
    img = imfinfo(srcfilesthermal{i});
    gps = img.GPSInfo
    lat = gps.GPSLatitude(1) + (gps.GPSLatitude(2)/60) + (gps.GPSLatitude(3)/3600)
    lon = gps.GPSLongitude(1) + (gps.GPSLongitude(2)/60) + (gps.GPSLongitude(3)/3600)
    
    %call exiftool
    %get all the images
    old_dir = cd; %store the current directory before it changes 
    cd('Images');
    files = dir('*.JPG');
    [status, cmdout] = system(['exiftool ' files(i).name]);
        
    if status ~= 0
        disp('Error in extracting image information');
        break
    end
        
    cmds = strsplit(cmdout, '\n');
    % Camera yaw
    yaw = strsplit(cmds{37}, ': ');
    yaw = str2double(yaw{2});
    alt = strsplit(cmds{35}, ': ');
    alt = str2double(alt{2});
    
    cd(old_dir); %restore the directory
    I = imread(srcfilesthermal{1});
    %transform the picture to yaw 0 degree
    [P,B] = TransformImage(I, yaw, 0);
    imshow(B);
    %map the transformed picture onto the blueprint
    RBG = imread(handles.Blueprint);
    RBG = DefectOnBluePrint(P,[lat lon], alt, RBG, frame, [-0.8 -2]); %added offset
    axes(handles.axes3)
    imshow(RBG);
end

%}
    
guidata(hObject, handles)



% --- Executes on button press in Import_blueprint.
function Import_blueprint_Callback(hObject, eventdata, handles)
% hObject    handle to Import_blueprint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = guidata(hObject);
filename = uigetfile('BlueprintBuilding\*.png');
I = ['BlueprintBuilding\' filename];
imageName = strsplit(filename, '.jpg');
if strcmp(imageName, filename)
    imageName = strsplit(filename, '.png');
end
handles.imageName = imageName{1};
handles.Blueprint = I;
guidata(hObject, handles);
axes(handles.axes3)
imshow(I);
hold on;


% --- Executes on button press in Defects.
function Defects_Callback(hObject, eventdata, handles)
% hObject    handle to Defects (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global images
uiopen('load');
axes(handles.axes3)
plot(images.X(1), images.Y(1), 'ko', 'MarkerSize', 10, 'LineWidth', 2);
